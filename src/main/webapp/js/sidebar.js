console.log(baseUrl);

var sideBar = new Vue({
	el : '#sidebar',
	data : {
		user : {},
		menuList : [],
		main : "welcome.html",
		password : '',
		newPassword : '',
		navTitle : "控制台"
	},
	methods : {
		getMenuList: function (event) {
			$.getJSON(baseUrl + "admin/resource/menus", function(r){
				sideBar.menuList = r;
				console.log(r);
				console.log($.now());
			});
		},
		getUser: function(){
			$.getJSON(baseUrl + "admin/user/info", function(r){
				sideBar.user = r;
				console.log(r);
			});
		}
	},
	created: function(){
		
	},
	updated: function(){
		
	}
});