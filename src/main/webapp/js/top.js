var vm = new Vue({
	el : '#top',
	data : {
		password : '',
		newPassword : '',
		renewPassword : ''
	},
	methods : {
		updatePassword : function() {
			layer.open({
				type : 1,
				skin : 'layui-layer-molv',
				title : "修改密码",
				area : [ '600px', '350px' ],
				shadeClose : false,
				content : jQuery("#passwordLayer"),
				btn : [ '修改', '取消' ],
				btn1 : function(index) {
					if(vm.newPassword != vm.renewPassword) {
						alert('两次输入的密码不一致');
						return;
					}

					var data = {
						password : vm.password,
						newPassword : vm.newPassword
					};
					$.ajax({
						type : "POST",
						url : baseUrl + "sec/passwd",
						data : JSON.stringify(data),
						dataType : "json",
						success : function(result) {
							if (result.code == 0) {
								layer.close(index);
								layer.alert('修改成功, 请重新登陆', function(index) {
									location.reload();
								});
							} else {
								layer.alert(result.message);
							}
						}
					});
				}
			});
		}
	},
});
