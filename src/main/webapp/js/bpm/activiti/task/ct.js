var bt;
var url = baseUrl + 'bpm/activiti/task/ct/';

// laydate({
// elem: '#birthday',
// //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许传入class、tag但必须按照这种方式 '#id .class'
// event: 'focus' //响应事件。如果没有传入event，则按照默认的click
// });

$(function() {
	// 初始化Table
	bt = new BootStrapTable();
	bt.init();
});

var BootStrapTable = function() {
	var dataTable = new Object();

	// 初始化Table
	dataTable.init = function() {
		$('#infoList')
				.bootstrapTable(
						{
							url : url + 'list', // 请求后台的URL（*）
							method : 'POST', // 请求方式（*）
							contentType : "application/x-www-form-urlencoded",
							// toolbar : '#toolbar', // 工具按钮用哪个容器
							striped : true, // 是否显示行间隔色
							cache : false, // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
							pagination : true, // 是否显示分页（*）
							sortable : false, // 是否启用排序

							sortOrder : "asc", // 排序方式
							pageNumber : 1, // 初始化加载第一页，默认第一页
							pageSize : 10, // 每页的记录行数（*）

							queryParams : dataTable.queryParams,// 传递参数（*）

							pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）

							strictSearch : false,

							search : false, // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端
							howColumns : false, // 是否显示所有的列
							showRefresh : false, // 是否显示刷新按钮
							showToggle : false, // 是否显示详细视图和列表视图的切换按钮

							minimumCountColumns : 2, // 最少允许的列数
							clickToSelect : true, // 是否启用点击选中行
							// height : 500, //
							// 行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
							uniqueId : "ID", // 每一行的唯一标识，一般为主键列

							cardView : false, // 是否显示详细视图
							detailView : false, // 是否显示父子表

							sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
							// 设置为undefined可以获取pageNumber，pageSize，searchText，sortName，sortOrder
							// 设置为limit可以获取limit, offset, search, sort, order
							queryParamsType : "undefined",
							responseHandler : function(res) {
								return {
									"rows" : res.content,
									"total" : res.totalElements
								};
							},
							// 数据列
							columns : [
									{
										radio : true
									},
									{
										title : "流程分类",
										field : "category",
										align : "left",
										valign : "center"
									},
									{
										title : "流程ID",
										field : "id",
										align : "left",
										valign : "center"
									},
									{
										title : "流程标识",
										field : "key",
										align : "left",
										valign : "center"
									},
									{
										title : "流程名称",
										field : "name",
										align : "center",
										valign : "left"
									},
									{
										title : "流程图片",
										field : "id",
										formatter : function(value, row, index) {
											return '<a target="_blank" href=pdDgrm?pdId='
													+ value
													+ '>'
													+ value
													+ '</a>';
										},
										align : "center",
										valign : "middle"
									},
									{
										title : "流程版本",
										field : "version",
										align : "center",
										valign : "center"
									},
									{
										title : "启动流程",
										field : "id",
										formatter : function(value, row, index) {
											return '<a href=sp?pdId=' + value + '><i class="fa fa-caret-square-o-right"></i></a>';
										},
										align : "center",
										valign : "middle"
									} ]
						});
	};

	// 得到查询的参数
	dataTable.queryParams = function(params) {
		var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
			pageNumber : params.pageNumber, // 页码
			pageSize : params.pageSize, // 页面大小
			sortName : params.sortName,
			sortOrder : params.sortOrder,

			pdCategory : $("#pdCategory").val(),
			pdName : $("#pdName").val()

		};
		return temp;
	};

	dataTable.refresh = function() {
		$('#infoList').bootstrapTable("refresh");
	};

	dataTable.getSelections = function() {
		var selections = $('#infoList').bootstrapTable('getSelections');
		return selections;
	};

	return dataTable;
};

var vm = new Vue(
		{
			el : '#infos',
			data : {
				showList : true,
				title : null,
				info : {}
			},
			methods : {
				query : function() {
					vm.reload();
				},

				deploy : function() {
					vm.showList = false;
					vm.title = '业务流程部署';

				},

				deployPd : function() {
					$.ajaxFileUpload({
						url : url + 'upload', // 需要链接到服务器地址
						secureuri : false,
						fileElementId : 'processFile', // 文件选择框的id属性
						dataType : 'text', // 服务器返回的格式，可以是json、xml
						success : function(data, status) // 相当于java中try语句块的用法
						{

							// $('#restoreDialog').html(data);

							alert("部署成功");
						},
						error : function(data, status, e) { // 相当于java中catch语句块的用法

							$('#restoreDialog').html("上传失败，请重试");
						}
					});

				},

				startProcess : function() {
					var selects = bt.getSelections();
					if (selects.length != 1) {
						alert("请选择要启动的流程");
						return;
					}

					vm.info = $.parseJSON(JSON.stringify(selects.shift()));

					// layer.open({
					// type : 2,
					// offset : '50px',
					// skin : 'layui-layer-molv',
					// title : vm.info.name + "的流程定义",
					// area : [ '800px', '600px' ],
					// shade : 0,
					// shadeClose : false,
					// content : 'sp?pdId=' + vm.info.id,
					// btn : [ '确定' ],
					// btn1 : function(index) {
					//
					// layer.close(index);
					// }
					// });

					$.ajax({
						url : 'sp?pdId=' + vm.info.id,
						context : document.body,
						success : function() {
							window.location.href = 'sp?pdId=' + vm.info.id;
						}
					});

				},
				del : function() {
					var selects = bt.getSelections();
					if (selects.length == 0) {
						alert("请选择要删除的业务流程定义");
						return;
					}

					confirm(
							"确定要删除所选的业务流程定义么?",
							function() {
								var ids = new Array();
								for (var i = 0; i < selects.length; i++) {
									ids[i] = $.parseJSON(JSON
											.stringify(selects[i])).deploymentId;
								}

								$
										.ajax({
											type : "POST",
											contentType : "application/json ; charset=utf-8",
											url : url + 'deletePds',
											data : JSON.stringify(ids),
											success : function(r) {
												if (r.code === 0) {
													alert('删除成功');
													vm.reload();
													vm.info = {};
													bt.refresh();
												} else {
													alert(r.message);
												}
											}
										});
							});
				},

				reload : function() {
					vm.showList = true;
					bt.refresh();
				}

			}
		});