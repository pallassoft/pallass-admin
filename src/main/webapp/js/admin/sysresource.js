var bt;
var url = baseUrl + 'admin/resource/';

$(function() {
	// 初始化Table
	bt = new BootStrapTable();
	bt.init();
});

var BootStrapTable = function() {
	var dataTable = new Object();

	// 初始化Table
	dataTable.init = function() {
		$('#infoList').bootstrapTable({
			url : url + 'list', // 请求后台的URL（*）
			method : 'POST', // 请求方式（*）
			contentType : "application/x-www-form-urlencoded",
			// toolbar : '#toolbar', // 工具按钮用哪个容器
			striped : true, // 是否显示行间隔色
			cache : false, // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
			pagination : true, // 是否显示分页（*）
			sortable : false, // 是否启用排序

			sortOrder : "asc", // 排序方式
			pageNumber : 1, // 初始化加载第一页，默认第一页
			pageSize : 10, // 每页的记录行数（*）

			queryParams : dataTable.queryParams,// 传递参数（*）

			pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）

			strictSearch : false,

			search : false, // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端
			howColumns : false, // 是否显示所有的列
			showRefresh : false, // 是否显示刷新按钮
			showToggle : false, // 是否显示详细视图和列表视图的切换按钮

			minimumCountColumns : 2, // 最少允许的列数
			clickToSelect : true, // 是否启用点击选中行
			// height : 500, //
			// 行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
			uniqueId : "ID", // 每一行的唯一标识，一般为主键列

			cardView : false, // 是否显示详细视图
			detailView : true, // 是否显示父子表

			sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
			// 设置为undefined可以获取pageNumber，pageSize，searchText，sortName，sortOrder
			// 设置为limit可以获取limit, offset, search, sort, order
			queryParamsType : "undefined",
			responseHandler : function(res) {
				return {
					"rows" : res.content,
					"total" : res.totalElements
				};
			},
			// 数据列
			columns : [ {
				checkbox : true
			}, {
				title : "资源名称",
				field : "resourceName",
				align : "center",
				valign : "middle"
			}, {
				title : "资源类型",
				field : "resourceType",
				formatter : function(value, row, index) {
					if (value == 0)
						return '<span class="label label-info">目录</span>';
					else if (value == 1)
						return '<span class="label label-primary">菜单</span>';
					else if (value == 2)
						return '<span class="label label-warning">按钮</span>';
				},
				align : "center",
				valign : "middle"
			}, {
				title : "资源URL",
				field : "resourceUrl",
				align : "left",
				valign : "middle"
			}, {
				title : "层级",
				field : "level",
				sortable : true,
				align : "center",
				valign : "middle"
			}, {
				title : "排序",
				field : "orderNum",
				sortable : true,
				align : "center",
				valign : "middle"
			}, {
				title : "图标",
				field : "icon",
				align : "center",
				formatter : function(value, row, index) {
					if (value == null)
						return '';
					else 
						return '<i class="' + value + ' fa-lg" aria-hidden="true"></i>';
				},
				valign : "middle"
			}, {
				title : "是否显示",
				sortable : true,
				field : "isHide",
				formatter : function(value, row, index) {
					if (value == 0)
						return '<span class="label label-info">显示</span>';
					else if (value == 1)
						return '<span class="label label-danger">隐藏</span>';
				},
				align : "center",
				valign : "middle"
			}, {
				title : "创建时间",
				field : "createTime",
				sortable : true,
				align : "center",
				valign : "middle"
			}, {
				title : "更新时间",
				field : "updateTime",
				sortable : true,
				align : "center",
				valign : "middle"
			} ]
		});
	};

	// 得到查询的参数
	dataTable.queryParams = function(params) {
		var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
			pageNumber : params.pageNumber, // 页码
			pageSize : params.pageSize, // 页面大小
			sortName : params.sortName,
			sortOrder : params.sortOrder,

			resourceName : $("#resourceName").val()

		};
		return temp;
	};

	dataTable.refresh = function() {
		$('#infoList').bootstrapTable("refresh");
	};

	dataTable.getSelections = function() {
		var selections = $('#infoList').bootstrapTable('getSelections');
		return selections;
	};

	return dataTable;
};

var setting = {
	data : {
		key : {
			name : "resourceName",
			url : "nourl"
		}
	}
};

var ztree;

var vm = new Vue({
	el : '#infos',
	data : {
		showList : true,
		title : null,
		info : {
			parent : null,
			resourceType : 1,
			orderNum : 1,
			level : 1
		}
	},
	methods : {
		query : function() {
			vm.reload();
		},
		initResourceTree : function(id) {
			$.get(url + "lists", function(r) {
				ztree = $.fn.zTree.init($("#rescTree"), setting, r.root);
				// 选中待添加/修改资源的父资源
				var node = ztree.getNodeByParam("id", vm.info.parentId);
				ztree.selectNode(node);				
				vm.info.parentName = node.resourceName;
			})
		},
		add : function() {
			vm.showList = false;
			vm.title = '添加资源';
			vm.info = {
				parent : {
					id : 0,
					resourceName : "根资源"
				},
				resourceType : 1,
				orderNum : 1,
				level : 1,
				parentId : 0,
				parentName : "根资源"
			};
			vm.initResourceTree();
		},
		update : function() {
			var selects = bt.getSelections();
			if (selects.length != 1) {
				alert("请选择要修改的角色");
				return;
			}
			vm.showList = false;
			vm.info = $.parseJSON(JSON.stringify(selects.shift()));
			vm.initResourceTree();
		},
		del : function() {
			var selects = bt.getSelections();
			if (selects.length == 0) {
				alert("请选择要删除的角色");
				return;
			}

			confirm("确定要删除所选的角色么?", function() {
				var ids = new Array();
				for (var i = 0; i < selects.length; i++) {
					ids[i] = $.parseJSON(JSON.stringify(selects[i])).id;
				}

				$.ajax({
					type : "POST",
					contentType : "application/json ; charset=utf-8",
					url : url + 'delete',
					data : JSON.stringify(ids),
					success : function(r) {
						if (r.code === 0) {
							layer.alert('删除成功');
							vm.reload();
							vm.info = {};
							bt.refresh();
						} else {
							alert(r.message);
						}
					}
				});
			});

		},
		saveOrUpdate : function() {
			var aUrl = vm.info.id == null ? url + "save" : url + "update";
			$.ajax({
				type : "POST",
				contentType : "application/json ; charset=utf-8",
				url : aUrl,
				data : JSON.stringify(vm.info),
				success : function(r) {
					if (r.code === 0) {
						alert('操作成功', function(index) {

						});
						vm.reload();
						vm.showList = true;
						vm.info = {};
						bt.refresh();
					} else {
						alert(r.message);
					}
				}
			});
		},
		reload : function() {
			vm.showList = true;
			bt.refresh();
		},
		resInfoTree : function() {

			layer.open({
				type : 1,
				offset : '50px',
				skin : 'layui-layer-molv',
				title : "选择所属上级资源",
				area : [ '300px', '450px' ],
				shade : 0,
				shadeClose : false,
				content : jQuery("#rescLayer"),
				btn : [ '确定', '取消' ],
				btn1 : function(index) {
					var node = ztree.getSelectedNodes();
					// 选择上级菜单
					vm.info.parent = $.parseJSON(JSON.stringify(node.shift()));

					vm.info.parentName = vm.info.parent.resourceName;
					vm.info.parentId = vm.info.parent.id;

					layer.close(index);
				}
			});
		}
	}
});