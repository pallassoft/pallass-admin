var bt;

var url = baseUrl + 'admin/role/';

$(function() {
	// 初始化Table
	bt = new BootStrapTable();
	bt.init();
});

var BootStrapTable = function() {
	var dataTable = new Object();

	// 初始化Table
	dataTable.init = function() {
		$('#infoList').bootstrapTable({
			url : url + 'list', // 请求后台的URL（*）
			method : 'POST', // 请求方式（*）
			contentType : "application/x-www-form-urlencoded",
			// toolbar : '#toolbar', // 工具按钮用哪个容器
			striped : true, // 是否显示行间隔色
			cache : false, // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
			pagination : true, // 是否显示分页（*）
			sortable : false, // 是否启用排序

			sortOrder : "asc", // 排序方式
			pageNumber : 1, // 初始化加载第一页，默认第一页
			pageSize : 10, // 每页的记录行数（*）

			queryParams : dataTable.queryParams,// 传递参数（*）

			pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）

			strictSearch : false,

			search : false, // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端
			howColumns : false, // 是否显示所有的列
			showRefresh : false, // 是否显示刷新按钮
			showToggle : false, // 是否显示详细视图和列表视图的切换按钮

			minimumCountColumns : 2, // 最少允许的列数
			clickToSelect : true, // 是否启用点击选中行
			// height : 500, // 行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
			uniqueId : "ID", // 每一行的唯一标识，一般为主键列

			cardView : false, // 是否显示详细视图
			detailView : false, // 是否显示父子表

			sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
			// 设置为undefined可以获取pageNumber，pageSize，searchText，sortName，sortOrder
			// 设置为limit可以获取limit, offset, search, sort, order
			queryParamsType : "undefined",
			responseHandler : function(res) {
				return {
					"rows" : res.content,
					"total" : res.totalElements
				};
			},
			// 数据列
			columns : [ {
				checkbox : true
			}, {
				title : "角色名称",
				field : "roleName",
				align : "center",
				valign : "middle"

			}, {
				title : "角色标识",
				field : "roleKey",
				align : "center",
				valign : "middle"
			}, {
				title : "状态",
				field : "status",
				formatter : function(value, row, index) {
					if (value == '0')
						return '<span class="label label-primary">正常</span>';
					return '<span class="label label-danger">禁用</span>';
				},
				align : "center",
				valign : "middle"
			}, {
				title : "创建时间",
				field : "createTime",
				sortable : true,
				align : "center",
				valign : "middle"
			}, {
				title : "更新时间",
				field : "updateTime",
				sortable : true,
				align : "center",
				valign : "middle"
			} ]
		});
	};

	// 得到查询的参数
	dataTable.queryParams = function(params) {
		var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
			pageNumber : params.pageNumber, // 页码
			pageSize : params.pageSize, // 页面大小
			sortName : params.sortName,
			sortOrder : params.sortOrder,

			roleName : $("#roleName").val(),

		};
		return temp;
	};

	dataTable.refresh = function() {
		$('#infoList').bootstrapTable("refresh");
	};

	dataTable.getSelections = function() {
		var selections = $('#infoList').bootstrapTable('getSelections');
		return selections;
	};

	return dataTable;
};

var setting = {
	data : {
		key : {
			name : "resourceName",
			url : "nourl"
		}
	},
	check : {
		enable : true,
		nocheckInherit : true,
		checked : "checked"
	}
};

var ztree;

var vm = new Vue({
	el : '#roles',
	data : {
		showList : true,
		title : null,
		role : {}
	},
	methods : {
		query : function() {
			vm.reload();
		},
		add : function() {
			vm.showList = false;
			vm.title = '添加角色';
			vm.role = {};
		},
		update : function() {
			var selects = bt.getSelections();
			if (selects.length != 1) {
				alert("请选择要修改的角色");
				return;
			}
			vm.showList = false;
			vm.role = $.parseJSON(JSON.stringify(selects.shift()));
		},
		del : function() {
			var selects = bt.getSelections();
			if (selects.length == 0) {
				alert("请选择要删除的角色");
				return;
			}

			confirm("确定要删除所选的角色么?", function() {
				var ids = new Array();
				for (var i = 0; i < selects.length; i++) {
					ids[i] = $.parseJSON(JSON.stringify(selects[i])).id;
				}

				$.ajax({
					type : "POST",
					contentType : "application/json ; charset=utf-8",
					url : url + 'delete',
					data : JSON.stringify(ids),
					success : function(r) {
						if (r.code === 0) {
							layer.alert('删除成功');
							vm.reload();
							vm.role = {};
							bt.refresh();
						} else {
							alert(r.message);
						}
					}
				});
			});

		},
		saveOrUpdate : function() {
			var aUrl = vm.role.id == null ? "save" : "update";
			aUrl = url + aUrl;
			$.ajax({
				type : "POST",
				contentType : "application/json ; charset=utf-8",
				url : aUrl,
				data : JSON.stringify(vm.role),
				success : function(r) {
					if (r.code === 0) {
						alert('操作成功', function(index) {

						});
						vm.reload();
						vm.showList = true;
						vm.role = {};
						bt.refresh();
					} else {
						alert(r.message);
					}
				}
			});
		},
		reload : function() {
			vm.showList = true;
			bt.refresh();
		},
		grant : function() {
			var selects = bt.getSelections();
			if (selects.length != 1) {
				alert("请选择要分配资源的角色");
				return;
			}
			vm.role = $.parseJSON(JSON.stringify(selects.shift()));

			$.get(url + "grant/" + vm.role.id, function(r) {
				ztree = $.fn.zTree.init($("#rescTree"), setting, r);
				ztree.expandAll(true);
			})

			var rtitle = "为" + vm.role.roleName + "分配资源";

			layer.open({
				type : 1,
				offset : '50px',
				skin : 'layui-layer-molv',
				title : rtitle,
				area : [ '300px', '450px' ],
				shade : 0,
				shadeClose : false,
				content : jQuery("#rescLayer"),
				btn : [ '确定', '取消' ],
				btn1 : function(index) {
					// 分配资源
					var nodes = ztree.getCheckedNodes(true);
					var ids = new Array();
					for (var i = 0; i < nodes.length; i++) {
						ids.push(nodes[i].id);
					}

					$.ajax({
						type : "POST",
						contentType : "application/json ; charset=utf-8",
						url : url + "grant/" + vm.role.id,
						dataType : "json",
						data : JSON.stringify(ids),
						success : function(r) {

							// layer.msg('分配成功', {time: 2000},function(){
							// var index =
							// parent.layer.getFrameIndex(window.name);
							// //先得到当前iframe层的索引
							// parent.layer.close(index);
							// });

							if (r.code === 0) {
								alert('分配操作成功', function(index) {
									layer.close(index);
								});

							} else {
								alert(r.message);
							}
						}
					});

					layer.close(index);
				}
			});
		}
	}
});