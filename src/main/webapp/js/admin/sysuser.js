var bt;
var url = baseUrl + 'admin/user/';

// laydate({
// elem: '#birthday',
// //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许传入class、tag但必须按照这种方式 '#id .class'
// event: 'focus' //响应事件。如果没有传入event，则按照默认的click
// });

$(function() {
	// 初始化Table
	bt = new BootStrapTable();
	bt.init();
});

var BootStrapTable = function() {
	var dataTable = new Object();

	// 初始化Table
	dataTable.init = function() {
		$('#infoList').bootstrapTable({
			url : url + 'list', // 请求后台的URL（*）
			method : 'POST', // 请求方式（*）
			contentType : "application/x-www-form-urlencoded",
			// toolbar : '#toolbar', // 工具按钮用哪个容器
			striped : true, // 是否显示行间隔色
			cache : false, // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
			pagination : true, // 是否显示分页（*）
			sortable : false, // 是否启用排序

			sortOrder : "asc", // 排序方式
			pageNumber : 1, // 初始化加载第一页，默认第一页
			pageSize : 10, // 每页的记录行数（*）

			queryParams : dataTable.queryParams,// 传递参数（*）

			pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）

			strictSearch : false,

			search : false, // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端
			howColumns : false, // 是否显示所有的列
			showRefresh : false, // 是否显示刷新按钮
			showToggle : false, // 是否显示详细视图和列表视图的切换按钮

			minimumCountColumns : 2, // 最少允许的列数
			clickToSelect : true, // 是否启用点击选中行
			// height : 500, //
			// 行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
			uniqueId : "ID", // 每一行的唯一标识，一般为主键列

			cardView : false, // 是否显示详细视图
			detailView : false, // 是否显示父子表

			sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
			// 设置为undefined可以获取pageNumber，pageSize，searchText，sortName，sortOrder
			// 设置为limit可以获取limit, offset, search, sort, order
			queryParamsType : "undefined",
			responseHandler : function(res) {
				return {
					"rows" : res.content,
					"total" : res.totalElements
				};
			},
			// 数据列
			columns : [ {
				checkbox : true
			}, {
				title : "账户名",
				field : "userName",
				align : "center",
				valign : "middle"
			}, {
				title : "昵称",
				field : "nickName",
				align : "center",
				valign : "middle"
			}, {
				title : "所属部门 ",
				field : "department.deptName",
				
				align : "center",
				valign : "middle"
			}, {
				title : "性别",
				field : "sex",
				formatter : function(value, row, index) {
					if (value == '0')
						return '<span class="label label-warning">女</span>';
					return '<span class="label label-primary">男</span>';
				},
				align : "center",
				valign : "middle"
			}, {
				title : "出生日期",
				field : "birthday",
				align : "center",
				valign : "middle"
			}, {
				title : "电话",
				field : "telephone",
				align : "center",
				valign : "middle"
			}, {
				title : "邮箱",
				field : "email",
				align : "center",
				valign : "middle"
			}, {
				title : "锁定",
				sortable : true,
				field : "locked",
				formatter : function(value, row, index) {
					if (value == '0')
						return '<span class="label label-info">正常</span>';
					return '<span class="label label-danger">禁用</span>';
				},
				align : "center",
				valign : "middle"
			} ]
		});
	};

	// 得到查询的参数
	dataTable.queryParams = function(params) {
		var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
			pageNumber : params.pageNumber, // 页码
			pageSize : params.pageSize, // 页面大小
			sortName : params.sortName,
			sortOrder : params.sortOrder,

			userName : $("#userName").val(),

		};
		return temp;
	};

	dataTable.refresh = function() {
		$('#infoList').bootstrapTable("refresh");
	};

	dataTable.getSelections = function() {
		var selections = $('#infoList').bootstrapTable('getSelections');
		return selections;
	};

	return dataTable;
};

var setting = {
	data : {
		key : {
			name : "roleName",
			url : "nourl"
		}
	},
	check : {
		enable : true,
		nocheckInherit : true,
		checked : "checked"
	}
};

var ztree;

var deptSetting = {
	data : {
		key : {
			name : "deptName",
			url : "nourl"
		}
	}
};

var deptZtree;

var vm = new Vue({
	el : '#infos',
	data : {
		showList : true,
		title : null,
		info : {
			department : {
				id : 0,
				deptName : "请选择所属部门"
			},
			personType : 0,
			locked : 0
		}
	},
	methods : {
		query : function() {
			vm.reload();
		},
		initDeptTree : function(id) {
			$.get(baseUrl + "org/dept/lists", function(r) {
				deptZtree = $.fn.zTree.init($("#deptTree"), deptSetting, r.root);
				// 选中待添加/修改资源的父资源
				var node = deptZtree.getNodeByParam("id", vm.info.department.id);
				if (node == null)
					return;
				deptZtree.selectNode(node);
				vm.info.department.deptName = node.deptName;
			})

		},
		selectDate : function() {
			var start = {
				min : '1900-01-01 00:00:00',
				max : laydate.now(),
				istoday : false,
				choose : function(datas) {
					vm.info.birthday = datas;
				}
			};
			start.elem = document.getElementById('birthday');
			layui.laydate(start);
		},
		add : function() {
			vm.showList = false;
			vm.title = '添加用户';
			vm.info = {
				// birthday : new Date(),
				department : {
					id : 0,
					deptName : "请选择所属部门"
				},
				personType : 1,
				deleteStatus : 0,
				locked : 0
			};

			vm.initDeptTree();
		},
		update : function() {
			vm.title = '修改用户';
			var selects = bt.getSelections();
			if (selects.length != 1) {
				alert("请选择要修改的用户");
				return;
			}
			vm.showList = false;
			vm.info = $.parseJSON(JSON.stringify(selects.shift()));
			if(vm.info.department == null) {
				var dept = {
					id : 0,
					deptName : "请选择所属部门"	
				}
				vm.info.department = $.parseJSON(JSON.stringify(dept));
			}
			vm.initDeptTree();
		},
		del : function() {
			var selects = bt.getSelections();
			if (selects.length == 0) {
				alert("请选择要删除的用户");
				return;
			}

			confirm("确定要删除所选的用户么?", function() {
				var ids = new Array();
				for (var i = 0; i < selects.length; i++) {
					ids[i] = $.parseJSON(JSON.stringify(selects[i])).id;
				}

				$.ajax({
					type : "POST",
					contentType : "application/json ; charset=utf-8",
					url : url + 'delete',
					data : JSON.stringify(ids),
					success : function(r) {
						if (r.code === 0) {
							alert('删除成功');
							vm.reload();
							vm.info = {};
							bt.refresh();
						} else {
							alert(r.message);
						}
					}
				});
			});
		},
		saveOrUpdate : function() {
			var aUrl = vm.info.id == null ? url + "save" : url + "update";
			$.ajax({
				type : "POST",
				contentType : "application/json ; charset=utf-8",
				url : aUrl,
				data : JSON.stringify(vm.info),
				success : function(r) {
					if (r.code === 0) {
						alert('操作成功', function(index) {

						});
						vm.reload();
						vm.showList = true;
						vm.info = {
							department : {
								id : 0,
								deptName : "请选择所属部门"
							},
							personType : 0,
							locked : 0
						};
						bt.refresh();
					} else {
						alert(r.message);
					}
				}
			});
		},
		resetpw : function() {
			
			var selects = bt.getSelections();
			if (selects.length != 1) {
				alert("请选择要重置的用户");
				return;
			}
			var user = $.parseJSON(JSON.stringify(selects.shift()));

			confirm("确定要重置所选的用户的密码么?", function() {	
				$.ajax({
					type : "POST",
					contentType : "application/json ; charset=utf-8",
					url : url + 'resetpw/' +  + user.id,
					success : function(r) {
						if (r.code === 0) {
							alert('重置成功');
							//bt.refresh();
						} else {
							alert(r.message);
						}
					}
				});
			});
		},
		reload : function() {
			vm.showList = true;
			bt.refresh();
		},
		deptInfoTree : function() {

			layer.open({
				type : 1,
				offset : '50px',
				skin : 'layui-layer-molv',
				title : "选择所属部门",
				area : [ '300px', '450px' ],
				shade : 0,
				shadeClose : false,
				content : jQuery("#deptLayer"),
				btn : [ '确定', '取消' ],
				btn1 : function(index) {
					var node = deptZtree.getSelectedNodes();
					// 选择上级菜单
					vm.info.department = $.parseJSON(JSON.stringify(node
							.shift()));

					layer.close(index);
				}
			});
		},
		grant : function() {
			var selects = bt.getSelections();
			if (selects.length != 1) {
				alert("请选择要分配角色的用户");
				return;
			}
			vm.info = $.parseJSON(JSON.stringify(selects.shift()));

			$.get(url + "grant/" + vm.info.id, function(r) {
				ztree = $.fn.zTree.init($("#roleTree"), setting, r);
				ztree.expandAll(true);
			})

			var rtitle = "为" + vm.info.userName + "分配角色";

			layer.open({
				type : 1,
				offset : '50px',
				skin : 'layui-layer-molv',
				title : rtitle,
				area : [ '300px', '450px' ],
				shade : 0,
				shadeClose : false,
				content : jQuery("#roleLayer"),
				btn : [ '确定', '取消' ],
				btn1 : function(index) {
					// 分配资源
					var nodes = ztree.getCheckedNodes(true);
					var ids = new Array();
					for (var i = 0; i < nodes.length; i++) {
						ids.push(nodes[i].id);
					}

					$.ajax({
						type : "POST",
						contentType : "application/json ; charset=utf-8",
						url : url + "grant/" + vm.info.id,
						dataType : "json",
						data : JSON.stringify(ids),
						success : function(r) {

							// layer.msg('分配成功', {time: 2000},function(){
							// var index =
							// parent.layer.getFrameIndex(window.name);
							// //先得到当前iframe层的索引
							// parent.layer.close(index);
							// });

							if (r.code === 0) {
								alert('分配操作成功', function(index) {
									layer.close(index);
								});

							} else {
								alert(r.message);
							}
						}
					});

					layer.close(index);
				}
			});
		}
	}
});