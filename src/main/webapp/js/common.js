//工具集合Tools
window.T = {};

// 获取请求参数
// 使用示例
// location.href = http://localhost:8080/index.html?id=123
// T.p('id') --> 123;
var url = function(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return unescape(r[2]);
	return null;
};
T.p = url;

// 全局配置
$.ajaxSetup({
	dataType : "json",
	contentType : "application/json",
	cache : false
});

// 从Layui导入layer
layui.use([ 'layer', 'form', 'laydate', 'upload' ], function() {
	var layer = layui.layer;
	var form = layui.form;
	var laydate = layui.laydate;
	var upload = layui.upload;
	// layer.msg(baseUrl);
});

// 重写alert
window.alert = function(msg, callback) {
	parent.layer.alert(msg, function(index) {
		parent.layer.close(index);
		if (typeof (callback) === "function") {
			callback("ok");
		}
	});
}

// 重写confirm式样框
window.confirm = function(msg, callback) {
	parent.layer.confirm(msg, {
		btn : [ '确定', '取消' ]
	}, function(index) {// 确定事件
		parent.layer.close(index);
		if (typeof (callback) === "function") {
			
			callback("ok");
		}
	}, function() {
	});
}

function getRootPath_web() {
    //获取当前网址，如： http://localhost:8083/padmin/sys/meun.html
    var curWwwPath = window.document.location.href;
    //获取主机地址之后的目录，如： padmin/sys/meun.html
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8083
    var localhostPaht = curWwwPath.substring(0, pos);
    //获取带"/"的项目名，如：/padmin
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return projectName;
}
