CREATE TABLE sys_role (
	id int(11) NOT NULL AUTO_INCREMENT,
	roleName varchar(255) DEFAULT NULL,
	roleKey varchar(255) DEFAULT NULL,
	status int(11) DEFAULT NULL,
	description varchar(255) DEFAULT NULL,
	createTime datetime DEFAULT NULL,
	updateTime datetime DEFAULT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色';
INSERT INTO sys_role VALUES
(1, '超级管理员', 'ADMIN', 0, '', now(), now()),
(2, '一般管理员', 'USER',  0, '', now(), now());


CREATE TABLE sys_resource (
	id int(11) NOT NULL AUTO_INCREMENT,
	resourceName varchar(255) DEFAULT NULL,
	resourceKey varchar(255) DEFAULT NULL,
	resourceUrl varchar(255) DEFAULT NULL,
	resourceType int(11) DEFAULT NULL,
	permissions varchar(255) DEFAULT NULL,
	resourceLevel int(11) DEFAULT NULL,
	orderNum int(11) DEFAULT NULL,
	icon varchar(255) DEFAULT NULL,
	isHide int(11) DEFAULT NULL,
	description varchar(255) DEFAULT NULL,
	createTime datetime DEFAULT NULL,
	updateTime datetime DEFAULT NULL,
	parent_id int(11) DEFAULT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_sys_resource_parent FOREIGN KEY (parent_id) REFERENCES sys_resource (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统资源';
INSERT INTO 
sys_resource 
(id,      resourceName, resourceKey, resourceUrl,   resourceType, permissions, resourceLevel, orderNum, icon, isHide, description, createTime, updateTime, parent_id)
VALUES

(1010000, '系统管理',    NULL, NULL,                       0, NULL,                     1, 1, 'fa fa-th-large',    0, NULL, now(), now(), NULL),

(1010100, '用户管理',    NULL, '/admin/user/index',        1, 'admin_user_index',       2, 1, 'fa fa-user',        0, NULL, now(), now(), 1010000),
(1010101, '用户查询',    NULL, '/admin/user/list',         2, 'admin_user_list',        3, 1, NULL,                0, NULL, now(), now(), 1010100),
(1010102, '用户添加',    NULL, '/admin/user/save',         2, 'admin_user_save',        3, 2, NULL,                0, NULL, now(), now(), 1010100),
(1010103, '用户修改',    NULL, '/admin/user/update',       2, 'admin_user_update',      3, 3, NULL,                0, NULL, now(), now(), 1010100),
(1010104, '用户删除',    NULL, '/admin/user/delete',       2, 'admin_user_delete',      3, 4, NULL,                0, NULL, now(), now(), 1010100),
(1010105, '用户密码重置', NULL, '/admin/user/resetpw',      2, 'admin_user_resetpw',     3, 5, NULL,                0, NULL, now(), now(), 1010100),
(1010106, '指派角色',    NULL, '/admin/user/grant',        2, 'admin_user_grant',       3, 6, NULL,                0, NULL, now(), now(), 1010100),

(1010200, '角色管理',    NULL, '/admin/role/index',        1, 'admin_role_index',       2, 2, 'fa fa-user-secret', 0, NULL, now(), now(), 1010000),
(1010201, '角色查询',    NULL, '/admin/role/list',         2, 'admin_role_list',        3, 1, NULL,                0, NULL, now(), now(), 1010200),
(1010202, '角色添加',    NULL, '/admin/role/save',         2, 'admin_role_save',        3, 2, NULL,                0, NULL, now(), now(), 1010200),
(1010203, '角色修改',    NULL, '/admin/role/upate',        2, 'admin_role_update',      3, 3, NULL,                0, NULL, now(), now(), 1010200),
(1010204, '角色删除',    NULL, '/admin/role/delete',       2, 'admin_role_delete',      3, 4, NULL,                0, NULL, now(), now(), 1010200),
(1010205, '分配资源',    NULL, '/admin/role/grant',        2, 'admin_role_grant',       3, 5, NULL,                0, NULL, now(), now(), 1010200),

(1010300, '资源管理',    NULL, '/admin/resource/index',    1, 'admin_resource_index',   2, 3, 'fa fa-th-list',     0, NULL, now(), now(), 1010000),
(1010301, '资源查询',    NULL, '/admin/resource/list',     2, 'admin_resource_list',    3, 1, NULL,                0, NULL, now(), now(), 1010300),
(1010302, '资源添加',    NULL, '/admin/resource/save',     2, 'admin_resource_save',    3, 2, NULL,                0, NULL, now(), now(), 1010300),
(1010303, '资源修改',    NULL, '/admin/resource/update',   2, 'admin_resource_update',  3, 3, NULL,                0, NULL, now(), now(), 1010300),
(1010304, '资源删除',    NULL, '/admin/resource/delete',   2, 'admin_resource_delete',  3, 4, NULL,                0, NULL, now(), now(), 1010300),

(1010400, 'SQL监控',    NULL, '/druid/index.html',        1, 'admin_sql_monitor',      2, 4, 'fa fa-bug',         0, NULL, now(), now(), 1010000),



(1020000, '组织结构',    NULL, NULL,                       0, NULL,                     1, 2, 'fa fa-university', 0, NULL, now(), now(), NULL),

(1020100, '部门管理',    NULL, '/org/dept/index',          1, 'org_dept_index',         2, 1, NULL,               0, NULL, now(), now(), 1020000),
(1020101, '部门查询',    NULL, '/org/dept/list',           2, 'org_dept_list',          3, 1, NULL,               0, NULL, now(), now(), 1020100),
(1020102, '部门添加',    NULL, '/org/dept/save',           2, 'org_dept_save',          3, 2, NULL,               0, NULL, now(), now(), 1020100),
(1020103, '部门修改',    NULL, '/org/dept/update',         2, 'org_dept_update',        3, 3, NULL,               0, NULL, now(), now(), 1020100),
(1020104, '部门删除',    NULL, '/org/dept/delete',         2, 'org_dept_delete',        3, 4, NULL,               0, NULL, now(), now(), 1020100),

(1020200, '班级管理',    NULL, '/org/clazz/index',         1, 'org_clazz_index',        2, 2, NULL,               0, NULL, now(), now(), 1020000),
(1020201, '班级查询',    NULL, '/org/clazz/list',          2, 'org_clazz_list',         3, 1, NULL,               0, NULL, now(), now(), 1020200),
(1020202, '班级添加',    NULL, '/org/clazz/save',          2, 'org_clazz_save',         3, 2, NULL,               0, NULL, now(), now(), 1020200),
(1020203, '班级修改',    NULL, '/org/clazz/update',        2, 'org_clazz_update',       3, 3, NULL,               0, NULL, now(), now(), 1020200),
(1020204, '班级删除',    NULL, '/org/clazz/delete',        2, 'org_clazz_delete',       3, 4, NULL,               0, NULL, now(), now(), 1020200),

(1020300, '教师管理',    NULL, '/org/teacher/index',       1, 'org_teacher_index',      2, 3, NULL,               0, NULL, now(), now(), 1020000),
(1020301, '教师查询',    NULL, '/org/teacher/list',        2, 'org_teacher_list',       3, 1, NULL,               0, NULL, now(), now(), 1020300),
(1020302, '教师添加',    NULL, '/org/teacher/save',        2, 'org_teacher_save',       3, 2, NULL,               0, NULL, now(), now(), 1020300),
(1020303, '教师修改',    NULL, '/org/teacher/update',      2, 'org_teacher_update',     3, 3, NULL,               0, NULL, now(), now(), 1020300),
(1020304, '教师删除',    NULL, '/org/teacher/delete',      2, 'org_teacher_delete',     3, 4, NULL,               0, NULL, now(), now(), 1020300),

(1020400, '学生管理',    NULL, '/org/student/index',       1, 'org_student_index',      2, 3, NULL,               0, NULL, now(), now(), 1020000),
(1020401, '学生查询',    NULL, '/org/student/list',        2, 'org_student_list',       3, 1, NULL,               0, NULL, now(), now(), 1020400),
(1020402, '学生添加',    NULL, '/org/student/save',        2, 'org_student_save',       3, 2, NULL,               0, NULL, now(), now(), 1020400),
(1020403, '学生修改',    NULL, '/org/student/update',      2, 'org_student_update',     3, 3, NULL,               0, NULL, now(), now(), 1020400),
(1020404, '学生删除',    NULL, '/org/student/delete',      2, 'org_student_delete',     3, 4, NULL,               0, NULL, now(), now(), 1020400),

(7010000, '任务管理',    NULL, NULL,                       0, NULL,                     1, 1, 'fa fa-th-large',    0, NULL, now(), now(), NULL),

(7010100, '任务发起',    NULL, '/bpm/activiti/task/ct/index',        1, 'bpm_act_task_ct_index',       2, 1, 'fa fa-user',        0, NULL, now(), now(), 7010000),
(7010101, '流程查询',    NULL, '/bpm/activiti/task/ct/list',         2, 'bpm_act_task_ct_list',        3, 1, NULL,                0, NULL, now(), now(), 7010100),
(7010102, '发起任务',    NULL, '/bpm/activiti/task/ct/sp',           2, 'bpm_act_task_ct_sp',          3, 2, NULL,                0, NULL, now(), now(), 7010100),

(7010200, '待办任务',    NULL, '/bpm/activiti/task/todo/index',      1, 'bpm_act_task_todo_index',       2, 1, 'fa fa-user',        0, NULL, now(), now(), 7010000),
(7010201, '流程查询',    NULL, '/bpm/activiti/task/todo/list',       2, 'bpm_act_task_todo_list',        3, 1, NULL,                0, NULL, now(), now(), 7010200),
(7010202, '签收任务',    NULL, '/bpm/activiti/task/todo/claim',      2, 'bpm_act_task_todo_claim',       3, 2, NULL,                0, NULL, now(), now(), 7010200),
(7010203, '办理任务',    NULL, '/bpm/activiti/task/todo/todo',       2, 'bpm_act_task_todo_todo',        3, 2, NULL,                0, NULL, now(), now(), 7010200),

(7010300, '已办任务',    NULL, '/bpm/activiti/task/history/index',   1, 'bpm_act_task_history_index',       2, 1, 'fa fa-user',        0, NULL, now(), now(), 7010000),
(7010301, '已办查询',    NULL, '/bpm/activiti/task/history/list',    2, 'bpm_act_task_history_list',        3, 1, NULL,                0, NULL, now(), now(), 7010300),



(8010000, '流程管理',    NULL, NULL,                       0, NULL,                     1, 1, 'fa fa-th-large',    0, NULL, now(), now(), NULL),

(8010100, '流程管理',    NULL, '/bpm/activiti/process/pd/index',        1, 'bpm_act_process_pd_index',       2, 1, 'fa fa-user',        0, NULL, now(), now(), 8010000),
(8010101, '流程查询',    NULL, '/bpm/activiti/process/pd/list',         2, 'bpm_act_process_pd_list',        3, 1, NULL,                0, NULL, now(), now(), 8010100),
(8010102, '流程明细',    NULL, '/bpm/activiti/process/pd/pdr',          2, 'bpm_act_process_pd_pdr',         3, 2, NULL,                0, NULL, now(), now(), 8010100),
(8010103, '流程部署',    NULL, '/bpm/activiti/process/pd/deployPd',     2, 'bpm_act_process_pd_deployPd',    3, 3, NULL,                0, NULL, now(), now(), 8010100),
(8010104, '删除流程',    NULL, '/bpm/activiti/process/pd/deletePds',    2, 'bpm_act_process_pd_deletePds',   3, 4, NULL,                0, NULL, now(), now(), 8010100),



(10020000, '基础数据',       NULL, NULL,                                 0, NULL,                               1, 1, 'fa fa-th-large',    0, NULL, now(), now(), NULL),

(10020100, '合同类型',       NULL, '/cm/base/contracttype/index',        1, 'cm_base_contracttype_index',       2, 1, 'fa fa-user',        0, NULL, now(), now(), 10020000),
(10020101, '合同类型查询',    NULL, '/cm/base/contracttype/list',         2, 'cm_base_contracttype_list',        3, 1, NULL,                0, NULL, now(), now(), 10020100),
(10020102, '合同类型添加',    NULL, '/cm/base/contracttype/save',         2, 'cm_base_contracttype_save',        3, 2, NULL,                0, NULL, now(), now(), 10020100),
(10020103, '合同类型修改',    NULL, '/cm/base/contracttype/update',       2, 'cm_base_contracttype_update',      3, 3, NULL,                0, NULL, now(), now(), 10020100),
(10020104, '合同类型删除',    NULL, '/cm/base/contracttype/delete',       2, 'cm_base_contracttype_delete',      3, 4, NULL,                0, NULL, now(), now(), 10020100),

(10020200, '业务类型',       NULL, '/cm/base/businesstype/index',        1, 'cm_base_businesstype_index',       2, 2, 'fa fa-user',        0, NULL, now(), now(), 10020000),
(10020201, '业务类型查询',    NULL, '/cm/base/businesstype/list',         2, 'cm_base_businesstype_list',        3, 1, NULL,                0, NULL, now(), now(), 10020200),
(10020202, '业务类型添加',    NULL, '/cm/base/businesstype/save',         2, 'cm_base_businesstype_save',        3, 2, NULL,                0, NULL, now(), now(), 10020200),
(10020203, '业务类型修改',    NULL, '/cm/base/businesstype/update',       2, 'cm_base_businesstype_update',      3, 3, NULL,                0, NULL, now(), now(), 10020200),
(10020204, '业务类型删除',    NULL, '/cm/base/businesstype/delete',       2, 'cm_base_businesstype_delete',      3, 4, NULL,                0, NULL, now(), now(), 10020200),

CREATE TABLE sys_role_resource (
	role_id int(11) NOT NULL,
	resource_id int(11) NOT NULL,
	PRIMARY KEY (role_id,resource_id),
	CONSTRAINT fk_role_resource_role_id FOREIGN KEY (role_id) REFERENCES sys_role(id),
	CONSTRAINT fk_role_resource_resource_id FOREIGN KEY (resource_id) REFERENCES sys_resource(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色资源关系';

INSERT INTO sys_role_resource
VALUES
(1, 1010000),
(1, 1010100), (1, 1010101), (1, 1010102), (1, 1010103), (1, 1010104), (1, 1010105), (1, 1010106),
(1, 1010200), (1, 1010201), (1, 1010202), (1, 1010203), (1, 1010204), (1, 1010205),
(1, 1010300), (1, 1010301), (1, 1010302), (1, 1010303), (1, 1010304);

CREATE TABLE sys_department (
	id int(11) NOT NULL AUTO_INCREMENT,
	deptNo varchar(255) DEFAULT NULL,
	deptName varchar(255) DEFAULT NULL,
	deptAddress varchar(255) DEFAULT NULL,
	telephone varchar(255) DEFAULT NULL,
	zipCode varchar(255) DEFAULT NULL,
	fax varchar(255) DEFAULT NULL,
	email varchar(255) DEFAULT NULL,
	locked int(11) DEFAULT NULL,
	description varchar(255) DEFAULT NULL,
	createTime datetime DEFAULT NULL,
	updateTime datetime DEFAULT NULL,
	parent_id int(11) DEFAULT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_sys_department_parent FOREIGN KEY (parent_id) REFERENCES sys_department (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门';

CREATE TABLE sys_user (
	id int(11) NOT NULL AUTO_INCREMENT,
	userName varchar(255) DEFAULT NULL,
	passwd varchar(255) DEFAULT NULL,
	personType int(11) NOT NULL,
	nickName varchar(255) DEFAULT NULL,
	idCard varchar(255) DEFAULT NULL,
	telephone varchar(255) DEFAULT NULL,
	email varchar(255) DEFAULT NULL,
	sex int(11) DEFAULT NULL,
	address varchar(255) DEFAULT NULL,
	birthday datetime DEFAULT NULL,
	deleteStatus int(11) DEFAULT NULL, 
	locked int(11) DEFAULT NULL,
	description varchar(255) DEFAULT NULL,
	createTime datetime DEFAULT NULL,
	updateTime datetime DEFAULT NULL,
	fk_dept int(11) DEFAULT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_user_department_id FOREIGN KEY (fk_dept) REFERENCES sys_department(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户';
INSERT INTO sys_user
VALUES
(1, 'admin', '$2a$10$WbXhTLeXsXpQuxMcHIT.5eTpfFJeg0VP1CFvsOronqT9s6cDXgn4q', 0, '超级用户', '', '', '', 1, '', now(), 0, 0, '', now(), now(), NULL);

CREATE TABLE sys_user_role (
	user_id int(11) NOT NULL,
	role_id int(11) NOT NULL,
	PRIMARY KEY (user_id, role_id),
	CONSTRAINT fk_user_role_user_id FOREIGN KEY (user_id) REFERENCES sys_user(id),
	CONSTRAINT fk_user_role_role_id FOREIGN KEY (role_id) REFERENCES sys_role(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色关系';
INSERT INTO sys_user_role
VALUES
(1, 1);