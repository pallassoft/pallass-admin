package com.houor.spring.security.web.intercept;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.util.AntPathMatcher;

/**
 * URL/permission implementation of <tt>FilterInvocationDefinitionSource</tt>.
 * Stores an ordered map of {@link String}s to <tt>ConfigAttribute</tt>
 * collections and provides matching of {@code FilterInvocation}s against the
 * items stored in the map.
 * 
 * @author Houor
 *
 * @createTime 2018-06-08 10:24
 *
 */
public class UrlFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

	private static Logger logger = LoggerFactory.getLogger(UrlFilterInvocationSecurityMetadataSource.class);

	private Map<String, List<ConfigAttribute>> configAttributeMap = new HashMap<>();

	AntPathMatcher antPathMatcher = new AntPathMatcher();

	public UrlFilterInvocationSecurityMetadataSource(Map<String, String> urlPermissonMap) {
		super();

		for (Map.Entry<String, String> up : urlPermissonMap.entrySet()) {
			logger.debug("Add Security Config: {} - {}", up.getKey(), up.getValue());
			configAttributeMap.put(up.getKey(), SecurityConfig.createList(up.getValue().split(",")));
		}

	}

	@Override
	public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {

		String requestUrl = ((FilterInvocation) object).getRequestUrl();

		for (Map.Entry<String, List<ConfigAttribute>> urlCa : configAttributeMap.entrySet()) {
			if (antPathMatcher.match(requestUrl, urlCa.getKey())) {
				return urlCa.getValue();
			}
		}

		return null;
	}

	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		logger.debug("get all ConfigAttributes");

		Set<ConfigAttribute> allAttributes = new HashSet<ConfigAttribute>();

		for (Map.Entry<String, List<ConfigAttribute>> entry : configAttributeMap.entrySet()) {
			allAttributes.addAll(entry.getValue());
		}

		return allAttributes;

	}

	@Override
	public boolean supports(Class<?> clazz) {
		return FilterInvocation.class.isAssignableFrom(clazz);
	}

}
