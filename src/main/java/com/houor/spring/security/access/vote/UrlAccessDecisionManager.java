package com.houor.spring.security.access.vote;

import java.util.Collection;
import java.util.Iterator;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;

/**
 * concrete implementation of
 * {@link org.springframework.security.access.AccessDecisionManager} that uses
 * requestURL/permission based approach.
 * 
 * @author Houor
 *
 * @createTime 2018-06-08 11:23
 *
 */
public class UrlAccessDecisionManager implements AccessDecisionManager {

	protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

	@Override
	public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {

		if (authentication == null) {
			throw new InsufficientAuthenticationException(messages.getMessage("AccessDecisionManager.insufficientAuthentication", "Authentication is insufficient"));
		}

		Iterator<ConfigAttribute> iterator = configAttributes.iterator();

		while (iterator.hasNext()) {
			ConfigAttribute configAttribute = iterator.next();

			Collection<? extends GrantedAuthority> grantedAuthorities = authentication.getAuthorities();
			for (GrantedAuthority authz : grantedAuthorities) {
				if (authz.getAuthority().equals(configAttribute.getAttribute())) {
					return;
				}
			}

		}

		throw new AccessDeniedException(messages.getMessage("AccessDecisionManager.accessDenied", "Access is denied"));

	}

	@Override
	public boolean supports(ConfigAttribute attribute) {
		return true;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

}
