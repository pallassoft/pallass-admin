package com.houor.bpm.activiti.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alibaba.fastjson.annotation.JSONField;
import com.houor.common.domain.ActivitiBaseDomain;

@Entity
@Table(name = "bpm_act_runtime_task")
public class ActRuntimeTask extends ActivitiBaseDomain implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8880013438087032681L;

	@Column(name = "NAME_")
	private String name;

	@Column(name = "CATEGORY_")
	private String category;

	@Column(name = "PARENT_TASK_ID_")
	private String parentTaskId;

	@Column(name = "TASK_DEF_KEY_")
	private String taskDefinitionKey;

	@Column(name = "FORM_KEY_")
	String formKey;

	@Column(name = "OWNER_")
	private String owner;

	@Column(name = "ASSIGNEE_")
	private String assignee;

	@Column(name = "GROUP_")
	private String group;

	@Column(name = "TYPE_")
	private String type;

	@Column(name = "DELEGATION_")
	private String delegationState;

	@Column(name = "EXECUTION_ID_")
	private String executionId;

	@Column(name = "PROC_INST_ID_")
	private String processInstanceId;

	@Column(name = "PROC_DEF_ID_")
	private String processDefinitionId;

	@Column(name = "DESCRIPTION_")
	private String description;

	@Column(name = "CREATE_TIME_")
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getParentTaskId() {
		return parentTaskId;
	}

	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}

	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}

	public String getFormKey() {
		return formKey;
	}

	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDelegationState() {
		return delegationState;
	}

	public void setDelegationState(String delegationState) {
		this.delegationState = delegationState;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
