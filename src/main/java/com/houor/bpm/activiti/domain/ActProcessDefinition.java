package com.houor.bpm.activiti.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.houor.common.domain.ActivitiBaseDomain;

/**
 * 流程定义
 * 
 * @author Houor
 *
 * @createTime: 2017-08-13 17:05
 */
@Entity
@Table(name = "bpm_act_processdefinition")
public class ActProcessDefinition extends ActivitiBaseDomain implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5800231382528824137L;

	@Column(name = "CATEGORY_")
	private String category;

	@Column(name = "VERSION_")
	private String version;

	@Column(name = "DEPLOYMENT_ID_")
	private String deploymentId;

	@Column(name = "NAME_")
	private String name;

	@Column(name = "KEY_")
	private String key;

	@Column(name = "RESOURCE_NAME_")
	private String resourceName;

	@Column(name = "DGRM_RESOURCE_NAME_")
	private String dargramName;

	@Column(name = "DESCRIPTION_")
	private String description;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getDargramName() {
		return dargramName;
	}

	public void setDargramName(String dargramName) {
		this.dargramName = dargramName;
	}

}
