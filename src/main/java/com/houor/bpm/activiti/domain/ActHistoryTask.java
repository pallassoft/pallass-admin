package com.houor.bpm.activiti.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alibaba.fastjson.annotation.JSONField;
import com.houor.common.domain.ActivitiBaseDomain;

@Entity
@Table(name = "bpm_act_history_task")
public class ActHistoryTask extends ActivitiBaseDomain implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8880013438087032681L;

	@Column(name = "NAME_")
	private String name;

	@Column(name = "CATEGORY_")
	private String category;

	@Column(name = "PARENT_TASK_ID_")
	private String parentTaskId;

	@Column(name = "TASK_DEF_KEY_")
	private String taskDefinitionKey;

	@Column(name = "FORM_KEY_")
	String formKey;

	@Column(name = "OWNER_")
	private String owner;

	@Column(name = "ASSIGNEE_")
	private String assignee;

	@Column(name = "EXECUTION_ID_")
	private String executionId;

	@Column(name = "PROC_INST_ID_")
	private String processInstanceId;

	@Column(name = "PROC_DEF_ID_")
	private String processDefinitionId;

	@Column(name = "PD_CATEGORY_")
	private String processDefinitionCategory;

	@Column(name = "PD_NAME_")
	private String processDefinitionName;

	@Column(name = "VERSION_")
	private String processDefinitionVersion;

	@Column(name = "DESCRIPTION_")
	private String description;

	@Column(name = "START_TIME_")
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;

	@Column(name = "CLAIM_TIME_")
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date claimTime;

	@Column(name = "END_TIME_")
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;

	@Column(name = "DURATION_")
	private int duration;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getParentTaskId() {
		return parentTaskId;
	}

	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}

	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}

	public String getFormKey() {
		return formKey;
	}

	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getClaimTime() {
		return claimTime;
	}

	public void setClaimTime(Date claimTime) {
		this.claimTime = claimTime;
	}

	public String getProcessDefinitionCategory() {
		return processDefinitionCategory;
	}

	public void setProcessDefinitionCategory(String processDefinitionCategory) {
		this.processDefinitionCategory = processDefinitionCategory;
	}

	public String getProcessDefinitionName() {
		return processDefinitionName;
	}

	public void setProcessDefinitionName(String processDefinitionName) {
		this.processDefinitionName = processDefinitionName;
	}

	public String getProcessDefinitionVersion() {
		return processDefinitionVersion;
	}

	public void setProcessDefinitionVersion(String processDefinitionVersion) {
		this.processDefinitionVersion = processDefinitionVersion;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

}
