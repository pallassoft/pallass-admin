package com.houor.bpm.activiti.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.houor.bpm.activiti.domain.ActRuntimeTask;
import com.houor.bpm.activiti.repository.RuntimeTaskRepository;
import com.houor.bpm.activiti.service.RuntimeTaskService;
import com.houor.common.repository.ActivitiBaseRepository;
import com.houor.common.service.ActivitiBaseServiceImpl;

@Service("runtimeTaskService")
public class RuntimeTaskServiceImpl extends ActivitiBaseServiceImpl<ActRuntimeTask, String> implements RuntimeTaskService {

	@Autowired
	private RuntimeTaskRepository runtimeTaskRepository;

	@Override
	public ActivitiBaseRepository<ActRuntimeTask, String> getBaseRepository() {
		return this.runtimeTaskRepository;
	}

}
