package com.houor.bpm.activiti.service;

import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

import com.houor.bpm.activiti.domain.ActProcessDefinition;
import com.houor.common.service.BaseService;

/**
 * 流程定义Service
 * 
 * @author Houor
 *
 */
public interface ProcessDefinitionService extends BaseService<ActProcessDefinition, String> {

	/**
	 * 读取流程定义XML或图片
	 * 
	 * @param pdId
	 *            流程定义ID
	 * @param resouceType
	 *            流程定义资源类型(xml, dgrm)
	 * 
	 * @return 资源流
	 */
	InputStream getProcessDefinitionResourceAsStream(String pdId, String resourceType);

	/**
	 * 根据流程部署信息ID删除流程定义
	 * 
	 * @param ids
	 *            流程部署信息
	 */
	void deleteProcessDefinitionByDeploymentIds(String[] ids);

	int deployProcessDefinition(MultipartFile file);

}
