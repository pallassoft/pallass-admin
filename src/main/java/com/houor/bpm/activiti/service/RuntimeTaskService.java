package com.houor.bpm.activiti.service;

import com.houor.bpm.activiti.domain.ActRuntimeTask;
import com.houor.common.service.BaseService;

public interface RuntimeTaskService extends BaseService<ActRuntimeTask, String> {

}
