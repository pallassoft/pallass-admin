package com.houor.bpm.activiti.service.impl;

import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipInputStream;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.houor.bpm.activiti.domain.ActProcessDefinition;
import com.houor.bpm.activiti.repository.ProcessDefinitionRepository;
import com.houor.bpm.activiti.service.ProcessDefinitionService;
import com.houor.common.repository.ActivitiBaseRepository;
import com.houor.common.service.ActivitiBaseServiceImpl;

@Service("processDefinitionService")
public class ProcessDefinitionServiceImpl extends ActivitiBaseServiceImpl<ActProcessDefinition, String> implements ProcessDefinitionService {

	private static Logger logger = LoggerFactory.getLogger(ProcessDefinitionServiceImpl.class);

	@Autowired
	private ProcessDefinitionRepository processDefinitionRepository;

	@Autowired
	RuntimeService runtimeService;

	@Autowired
	RepositoryService repositoryService;

	@Override
	public ActivitiBaseRepository<ActProcessDefinition, String> getBaseRepository() {
		return this.processDefinitionRepository;
	}

	@Override
	public InputStream getProcessDefinitionResourceAsStream(String pdId, String resourceType) {

		ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionId(pdId).singleResult();

		String resourceName = "";

		if ("xml".equals(resourceType)) {
			resourceName = processDefinition.getResourceName();
		} else if ("dgrm".equals(resourceType)) {
			resourceName = processDefinition.getDiagramResourceName();
		}

		InputStream resourceAsStream = repositoryService.getResourceAsStream(processDefinition.getDeploymentId(), resourceName);
		return resourceAsStream;

	}

	@Override
	public void deleteProcessDefinitionByDeploymentIds(String[] ids) {
		for (String id : ids) {
			repositoryService.deleteDeployment(id, true);
		}
	}

	@Override
	public int deployProcessDefinition(MultipartFile file) {

		String fileName = file.getOriginalFilename();
		String baseName = FilenameUtils.getBaseName(fileName);
		String extension = FilenameUtils.getExtension(fileName);

		try {
			InputStream fileInputStream = file.getInputStream();
			Deployment deployment = null;
			if (extension.equals("zip")) {
				ZipInputStream zip = new ZipInputStream(fileInputStream);
				deployment = repositoryService.createDeployment().addZipInputStream(zip).category(baseName).deploy();
			} else if (extension.equals("bpmn")) {
				deployment = repositoryService.createDeployment().addInputStream(fileName, fileInputStream).category(baseName).deploy();
			} else {
				logger.debug("不支持的流程定义文件类型：" + extension);
			}

			if (deployment == null) {
				return 0;
			}

			List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).list();

			// 设置流程分类
			for (ProcessDefinition processDefinition : list) {
				repositoryService.setProcessDefinitionCategory(processDefinition.getId(), baseName);
			}

			return list.size();

		} catch (Exception e) {
			throw new ActivitiException("部署失败！", e);
		}

	}

}
