package com.houor.bpm.activiti.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.houor.bpm.activiti.domain.ActHistoryTask;
import com.houor.bpm.activiti.repository.HistoryTaskRepository;
import com.houor.bpm.activiti.service.HistoryTaskService;
import com.houor.common.repository.ActivitiBaseRepository;
import com.houor.common.service.ActivitiBaseServiceImpl;

@Service("historyTaskService")
public class HistoryTaskServiceImpl extends ActivitiBaseServiceImpl<ActHistoryTask, String> implements HistoryTaskService {

	@Autowired
	private HistoryTaskRepository historyTaskRepository;

	@Override
	public ActivitiBaseRepository<ActHistoryTask, String> getBaseRepository() {
		return this.historyTaskRepository;
	}

}
