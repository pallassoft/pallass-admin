package com.houor.bpm.activiti.service;

import com.houor.bpm.activiti.domain.ActHistoryTask;
import com.houor.common.service.BaseService;

public interface HistoryTaskService extends BaseService<ActHistoryTask, String> {

}
