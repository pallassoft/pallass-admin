package com.houor.bpm.activiti.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.houor.bpm.activiti.domain.ActHistoryTask;
import com.houor.bpm.activiti.service.HistoryTaskService;
import com.houor.common.controller.BaseController;
import com.houor.common.data.jpa.criteria.OperatorType;
import com.houor.common.data.jpa.domain.ListSpecificationBuilder;
import com.pallass.security.util.SecurityUtil;

@Controller
@RequestMapping("/bpm/activiti/task/history")
public class TaskHistoryController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(TaskHistoryController.class);

	@Autowired
	private HistoryTaskService historyTaskService;

	@RequestMapping(value = "/index")
	public String index(Model model) {
		model.addAttribute("purl", "bpm/activiti/task/history");
		return "bpm/activiti/task/history";
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public Page<ActHistoryTask> list() {
		logger.debug("retrieive history task...");
		ListSpecificationBuilder<ActHistoryTask> builder = new ListSpecificationBuilder<ActHistoryTask>();

		String category = request.getParameter("taskCategory");
		if (StringUtils.isNotBlank(category)) {
			builder.add("category", OperatorType.LIKE, category);
		}

		String name = request.getParameter("taskName");
		if (StringUtils.isNotBlank(name)) {
			builder.add("name", OperatorType.LIKE, name);
		}

		String pdName = request.getParameter("pdName");
		if (StringUtils.isNotBlank(pdName)) {
			builder.add("processDefinitionName", OperatorType.LIKE, pdName);
		}

		builder.add("assignee", OperatorType.LIKE, SecurityUtil.getCurrentUserLoginName());

		return historyTaskService.findAll(builder.generateListSpecification(), getPageRequest());
	}

}
