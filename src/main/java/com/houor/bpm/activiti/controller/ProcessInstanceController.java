package com.houor.bpm.activiti.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.houor.common.controller.BaseController;

/**
 * @author Houor
 *
 */
@Controller
@RequestMapping("/bpm/activiti/process/pi")
public class ProcessInstanceController extends BaseController {

}
