package com.houor.bpm.activiti.controller;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import com.houor.bpm.activiti.domain.ActProcessDefinition;
import com.houor.bpm.activiti.service.ProcessDefinitionService;
import com.houor.common.controller.BaseController;
import com.houor.common.data.jpa.criteria.OperatorType;
import com.houor.common.data.jpa.domain.ListSpecificationBuilder;
import com.houor.common.utils.BpmTaskUtils;

/**
 * 任务创建Controller
 * 
 * @author Houor
 *
 * @createTime 2017-08-15 17:10
 */
@Controller
@RequestMapping("/bpm/activiti/task/ct")
public class TaskCreateController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(ProcessDefinitionController.class);

	@Autowired
	private ProcessDefinitionService processDefinitionService;

	@Autowired
	private BpmTaskUtils bpmTaskUtils;

	@RequestMapping(value = "/index")
	public String index(Model model) {
		model.addAttribute("purl", "bpm/activiti/task/ct");
		return "bpm/activiti/task/ct";
	}

	/**
	 * 获取流程定义列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public Page<ActProcessDefinition> list() {
		logger.debug("retrieive process defintions...");
		ListSpecificationBuilder<ActProcessDefinition> builder = new ListSpecificationBuilder<ActProcessDefinition>();
		String category = request.getParameter("pdCategory");
		if (StringUtils.isNotBlank(category)) {
			builder.add("category", OperatorType.LIKE, category);
		}

		String name = request.getParameter("pdName");
		if (StringUtils.isNotBlank(name)) {
			builder.add("name", OperatorType.LIKE, name);
		}

		String key = request.getParameter("pdKey");
		if (StringUtils.isNotBlank(key)) {
			builder.add("key", OperatorType.LIKE, key);
		}

		return processDefinitionService.findAll(builder.generateListSpecification(), getPageRequest());
	}

	/**
	 * 启动流程
	 * 
	 * 转向表单
	 * 
	 * @param pdId
	 *            流程定义ID
	 * @return
	 */
	@RequestMapping(value = "/sp", method = RequestMethod.GET)
	public String startProcess(String pdId, RedirectAttributesModelMap modelMap) {

		String startFormKey = bpmTaskUtils.getStartFormKey(pdId);
		modelMap.addAttribute("processDefinitionId", pdId);
		modelMap.addAttribute("status", "start");
		return redirect(startFormKey);
	}

	/**
	 * 读取流程定义图片
	 * 
	 * @param pdId
	 *            流程定义ID
	 * @throws IOException
	 */
	@RequestMapping(value = "/pdDgrm", method = RequestMethod.GET)
	@ResponseBody
	public void processDefinitionResoucesDgrm(String pdId) throws IOException {
		InputStream pdRresouceInputStream = processDefinitionService.getProcessDefinitionResourceAsStream(pdId, "dgrm");

		byte[] b = new byte[1024];
		int len = -1;

		response.setContentType("image/jpeg");

		while ((len = pdRresouceInputStream.read(b, 0, 1024)) != -1) {
			response.getOutputStream().write(b, 0, len);
		}
		response.getOutputStream().flush();
		pdRresouceInputStream.close();
	}

}
