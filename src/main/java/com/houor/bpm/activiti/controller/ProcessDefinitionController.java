package com.houor.bpm.activiti.controller;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.houor.bpm.activiti.domain.ActProcessDefinition;
import com.houor.bpm.activiti.service.ProcessDefinitionService;
import com.houor.common.controller.BaseController;
import com.houor.common.data.jpa.criteria.OperatorType;
import com.houor.common.data.jpa.domain.ListSpecificationBuilder;
import com.pallass.utils.RequestResult;

/**
 * 流程定义Process Definition Controller
 * 
 * @author Houor
 *
 */
@Controller
@RequestMapping("/bpm/activiti/process/pd")
public class ProcessDefinitionController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(ProcessDefinitionController.class);

	@Autowired
	private ProcessDefinitionService processDefinitionService;

	@RequestMapping(value = "/index")
	public String index(Model model) {
		model.addAttribute("purl", "bpm/activiti/process/pd");
		return "bpm/activiti/process/pd";
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public Page<ActProcessDefinition> list() {
		logger.debug("retrieive process defintions...");
		ListSpecificationBuilder<ActProcessDefinition> builder = new ListSpecificationBuilder<ActProcessDefinition>();
		String category = request.getParameter("pdCategory");
		if (StringUtils.isNotBlank(category)) {
			builder.add("category", OperatorType.LIKE, category);
		}

		String name = request.getParameter("pdName");
		if (StringUtils.isNotBlank(name)) {
			builder.add("name", OperatorType.LIKE, name);
		}

		String key = request.getParameter("pdKey");
		if (StringUtils.isNotBlank(key)) {
			builder.add("key", OperatorType.LIKE, key);
		}

		return processDefinitionService.findAll(builder.generateListSpecification(), getPageRequest());
	}

	/**
	 * 读取流程定义XML或图片
	 * 
	 * @param pdId
	 *            流程定义ID
	 * @param resourceType
	 *            流程定义资源类型(xml, dgrm)
	 * @throws IOException
	 */
	@RequestMapping(value = "/pdr", method = RequestMethod.GET)
	@ResponseBody
	public void processDefinitionResouces(String pdId, String resourceType) throws IOException {
		InputStream pdRresouceInputStream = processDefinitionService.getProcessDefinitionResourceAsStream(pdId, resourceType);

		byte[] b = new byte[1024];
		int len = -1;

		if ("dgrm".equals(resourceType)) {
			response.setContentType("image/jpeg");
		} else if("xml".equals(resourceType)) {
			response.setContentType("text/plain");
		}

		while ((len = pdRresouceInputStream.read(b, 0, 1024)) != -1) {
			response.getOutputStream().write(b, 0, len);
		}
		response.getOutputStream().flush();
		pdRresouceInputStream.close();

	}

	@RequestMapping(value = "/deletePds", method = RequestMethod.POST)
	@ResponseBody
	public RequestResult deleteProcessDefinitionByDeploymentIds(@RequestBody String ids[]) {
		processDefinitionService.deleteProcessDefinitionByDeploymentIds(ids);
		return RequestResult.success("删除流程定义成功");
	}

	@RequestMapping(method = RequestMethod.POST, path = "/deployPd")
	public String deployPd(@RequestPart("file") MultipartFile file) {

		processDefinitionService.deployProcessDefinition(file);

		return "bpm/activiti/process/pd";
	}

	@RequestMapping(method = RequestMethod.GET, path = "/pdDetails")
	public String processDefinitionDetails(String pdId) {

		return "bpm/activiti/process/pdDetails";
	}

}
