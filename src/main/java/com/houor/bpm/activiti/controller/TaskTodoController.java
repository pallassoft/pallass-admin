package com.houor.bpm.activiti.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import com.houor.bpm.activiti.domain.ActRuntimeTask;
import com.houor.bpm.activiti.service.RuntimeTaskService;
import com.houor.common.controller.BaseController;
import com.houor.common.data.jpa.criteria.OperatorType;
import com.houor.common.data.jpa.domain.ListSpecificationBuilder;
import com.houor.common.utils.BpmTaskUtils;
import com.pallass.admin.service.SysUserService;
import com.pallass.security.util.SecurityUtil;
import com.pallass.utils.RequestResult;

/**
 * 任务执行Controller
 * 
 * @author Houor
 *
 * @createTime 2017-08-17 10:14
 */
@Controller
@RequestMapping("/bpm/activiti/task/todo")
public class TaskTodoController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(TaskTodoController.class);

	@Autowired
	private BpmTaskUtils bpmTaskUtils;

	@Autowired
	private RuntimeTaskService runtimeTaskService;

	@Autowired
	private SysUserService userService;

	@RequestMapping(value = "/index")
	public String index(Model model) {
		model.addAttribute("purl", "bpm/activiti/task/todo");
		return "bpm/activiti/task/todo";
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public Page<ActRuntimeTask> list() {
		logger.debug("retrieive runtime task...");
		ListSpecificationBuilder<ActRuntimeTask> builder = new ListSpecificationBuilder<ActRuntimeTask>();

		String category = request.getParameter("taskCategory");
		if (StringUtils.isNotBlank(category)) {
			builder.add("category", OperatorType.LIKE, category);
		}

		String name = request.getParameter("taskName");
		if (StringUtils.isNotBlank(name)) {
			builder.add("name", OperatorType.LIKE, name);
		}

		// 待签收, 待办, 全部任务
		String taskType = request.getParameter("taskType");
		if (StringUtils.isNotBlank(taskType)) {
			// 待办
			if ("1".equals(taskType)) {
				// 属于登陆用户的任务
				builder.add("assignee", OperatorType.LIKE, SecurityUtil.getCurrentUserLoginName());
			}
			// 待签收
			else if ("2".equals(taskType)) {
				// 属于登陆用户的候选任务
				List<String> roleKeys = userService.getRoleKeyByUserName(SecurityUtil.getCurrentUserLoginName());
				if (!roleKeys.isEmpty()) {
					builder.add("assignee", OperatorType.ISNULL, null);
					builder.add("group", OperatorType.STRINGIN, StringUtils.join(roleKeys, ","));
				}
			}
			// 全部
			else {
				// 属于登陆用户的候选任务
				// 属于登陆用户的任务

				List<String> roleKeys = userService.getRoleKeyByUserName(SecurityUtil.getCurrentUserLoginName());
				if (!roleKeys.isEmpty()) {
					builder.add("group", OperatorType.STRINGIN, StringUtils.join(roleKeys, ","));
					builder.add("assignee", OperatorType.ISNULL, null);
				}

				builder.or("assignee", OperatorType.LIKE, SecurityUtil.getCurrentUserLoginName());
			}

		}

		return runtimeTaskService.findAll(builder.generateListSpecification(), getPageRequest());
	}

	@RequestMapping(value = "/claimTask", method = RequestMethod.POST)
	@ResponseBody
	public RequestResult claimTask(@RequestBody String[] taskIds) {

		String userId = SecurityUtil.getCurrentUserLoginName();

		String result = bpmTaskUtils.claimTask(taskIds, userId);
		if (StringUtils.isBlank(result)) {
			return RequestResult.success("任务签收成功");
		}

		return RequestResult.failure(result);
	}

	/**
	 * 办理任务
	 * 
	 * @param pdId
	 *            流程定义ID
	 * @return
	 */
	@RequestMapping(value = "/todo", method = RequestMethod.GET)
	public String todo(String taskId, RedirectAttributesModelMap modelMap) {

		Map<String, String> urlProperties = new HashMap<>();
		String formKey = bpmTaskUtils.getTaskFormKey(taskId, urlProperties);
		modelMap.addAttribute("processDefinitionId", urlProperties.get("processDefinitionId"));
		modelMap.addAttribute("taskDefinitionKey", urlProperties.get("taskDefinitionKey"));
		modelMap.addAttribute("taskId", taskId);
		modelMap.addAttribute("status", "todo");

		return redirect(formKey);
	}

}
