package com.houor.bpm.activiti.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.houor.common.controller.BaseController;

/**
 * 流程定义模型Process Model Controller
 * 
 * @author Houor
 * 
 * @createTime 2017-08-15 17:03
 *
 */
@Controller
@RequestMapping("/bpm/activiti/process/pm")
public class ProcessModelController extends BaseController {

}
