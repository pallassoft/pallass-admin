package com.houor.bpm.activiti.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ActivitiTaskAlreadyClaimedException;
import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.FormType;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.impl.form.EnumFormType;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.houor.common.domain.BpmBaseDomain;
import com.houor.common.utils.BpmTaskUtils;

/**
 * 
 * 业务流程任务工具类
 * 
 * @author Houor
 *
 * @createTime 2017-08-18 12:31
 */
public class BpmTaskUtilsImpl implements BpmTaskUtils {

	private static Logger logger = LoggerFactory.getLogger(BpmTaskUtilsImpl.class);

	@Autowired
	RepositoryService repositoryService;

	@Autowired
	RuntimeService runtimeService;

	@Autowired
	FormService formService;

	@Autowired
	TaskService taskService;

	@Autowired
	IdentityService identityService;

	@Autowired
	HistoryService historyService;

	/**
	 * 根据流程定义ID启动流程
	 * 
	 * @param processDefinitionId
	 * @return
	 */
	public ProcessInstance startProcess(String processDefinitionId) {
		return runtimeService.startProcessInstanceById(processDefinitionId);
	}

	/**
	 * 启动流程
	 * 
	 * @param userId
	 * @param processDefinitionId
	 * @param businessObject
	 * @param values
	 * @return
	 */
	@Override
	public ProcessInstance startProcess(String userId, String processDefinitionId, BpmBaseDomain businessObject, Map<String, Object> values) {

		identityService.setAuthenticatedUserId(userId);

		if (values == null) {
			values = new HashMap<>();
			values.put("businessObject", businessObject);
		}

		String businessKey = businessObject.getClass().getName() + ":" + businessObject.getId().toString();

		ProcessInstance pi = runtimeService.startProcessInstanceById(processDefinitionId, businessKey, values);

		identityService.setAuthenticatedUserId("");

		return pi;
	}

	/**
	 * 根据流程实例ID获取流程实例
	 * 
	 * @param processDefinitionId
	 * @return
	 */
	public ProcessInstance getProcessInstance(String processInstanceId) {
		return runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
	}

	/**
	 * 获取流程开始表单
	 * 
	 * @param processDefinitionId
	 * @return
	 */
	public String getStartFormKey(String processDefinitionId) {

		String startFormKey = "";
		if (StringUtils.isNotBlank(processDefinitionId)) {
			startFormKey = formService.getStartFormKey(processDefinitionId);
			if (StringUtils.isBlank(startFormKey)) {
				startFormKey = "/404";
			}
		}
		logger.debug("Get process {} start form key: {}", processDefinitionId, startFormKey);
		return startFormKey;
	}

	/**
	 * 获取流程任务表单
	 * 
	 * @param processDefinitionId
	 * @param taskDefinitionKey
	 * @return
	 */
	public String getTaskFormKey(String processDefinitionId, String taskDefinitionKey) {

		String formKey = "";
		if (StringUtils.isNotBlank(processDefinitionId)) {
			if (StringUtils.isNotBlank(taskDefinitionKey)) {
				try {
					formKey = formService.getTaskFormKey(processDefinitionId, taskDefinitionKey);
				} catch (Exception e) {
					formKey = "";
				}
			}

			// 如果任务表单为空, 则使用开始节点表单
			if (StringUtils.isNotBlank(processDefinitionId)) {
				formKey = formService.getStartFormKey(processDefinitionId);
			}

			if (StringUtils.isBlank(formKey)) {
				formKey = "/404";
			}
		}
		logger.debug("Get process {}, task {} form key: {}", processDefinitionId, taskDefinitionKey, formKey);
		return formKey;
	}

	/**
	 * 获取流程任务表单
	 * 
	 * @param taskId
	 * @return
	 */
	@Override
	public String getTaskFormKey(String taskId) {
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
		return getTaskFormKey(task.getProcessDefinitionId(), task.getTaskDefinitionKey());
	}

	/**
	 * 获取流程任务表单url及url属性
	 * 
	 * @param taskId
	 * @param urlProperties
	 * @return
	 */
	@Override
	public String getTaskFormKey(String taskId, Map<String, String> urlProperties) {
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();

		urlProperties.put("processDefinitionId", task.getProcessDefinitionId());
		urlProperties.put("taskDefinitionKey", task.getTaskDefinitionKey());
		urlProperties.put("taskId", taskId);

		return getTaskFormKey(task.getProcessDefinitionId(), task.getTaskDefinitionKey());
	}

	/**
	 * 签收任务
	 * 
	 * @param taskIds
	 * @param userId
	 * @return
	 */
	@Override
	public String claimTask(String[] taskIds, String userId) {

		List<String> claimed = new ArrayList<>();
		List<String> unclaimed = new ArrayList<>();

		for (String taskId : taskIds) {

			try {
				taskService.claim(taskId, userId);
				claimed.add(taskId);
			} catch (ActivitiTaskAlreadyClaimedException e) {
				logger.debug("Task: {} is already claimed by another uesr.", taskId);
				unclaimed.add(taskId);
			}
		}

		if (unclaimed.isEmpty()) {
			return "";
		} else {
			StringBuffer sb = new StringBuffer();
			sb.append(StringUtils.join(claimed, ", ")).append("签收成功, ").append(StringUtils.join(unclaimed, ", ")).append("被其他用户签收");
			return sb.toString();
		}

	}

	/**
	 * 获取并设置任务信息
	 * 
	 * @param domain
	 */
	@Override
	public int handleTaskInfo(BpmBaseDomain domain) {

		TaskFormData fd = formService.getTaskFormData(domain.getTaskId());

		// 从流程变量内获取保存的jpa数据
		// BpmBaseDomain bbd = runtimeService.getVariable(fd.getTask().getExecutionId(),
		// "businessObject", domain.getClass());

		// 获取业务对象ID
		ProcessInstance pi = runtimeService.createProcessInstanceQuery().processInstanceId(fd.getTask().getProcessInstanceId()).singleResult();
		String businessObjectId = StringUtils.substringAfterLast(pi.getBusinessKey(), ":");
		domain.setId(Integer.parseInt(businessObjectId));

		// 约定: 任务审批走向变量信息 == taskDefinitionKey + "Audit"
		domain.setAuditValueName(fd.getTask().getTaskDefinitionKey() + "Audit");
		List<FormProperty> fpList = fd.getFormProperties();
		for (FormProperty fp : fpList) {
			if (fp.getId().equals(domain.getAuditValueName())) {
				FormType ft = fp.getType();
				// 获取任务的审批枚举变量信息
				if (ft instanceof EnumFormType) {
					@SuppressWarnings("unchecked")
					Map<String, String> transitionConditionMap = (Map<String, String>) ft.getInformation("values");
					domain.setTransitionConditionMap(transitionConditionMap);
				}
			}

		}
		logger.debug("Task: {}, Audit Variable: {}, Transition Info: {}", fd.getTask().getName(), domain.getAuditValueName(),
				domain.getTransitionConditionMap() == null ? "" : domain.getTransitionConditionMap().toString());
		return 0;

	}

	/**
	 * 完成任务
	 * 
	 * @param taskId
	 * @param processInstanceId
	 * @param comment
	 *            意见
	 * @param variables
	 */
	@Override
	public void complete(String taskId, String processInstanceId, String comment, Map<String, Object> variables) {
		// 添加意见
		if (StringUtils.isNotBlank(processInstanceId) && StringUtils.isNotBlank(comment)) {
			taskService.addComment(taskId, processInstanceId, comment);
		}

		// 提交任务
		if (variables == null) {
			taskService.complete(taskId);
		} else {

			taskService.complete(taskId, variables);
		}
	}

}
