package com.houor.bpm.activiti.repository;

import org.springframework.stereotype.Repository;

import com.houor.bpm.activiti.domain.ActProcessDefinition;
import com.houor.common.repository.ActivitiBaseRepository;

@Repository
public interface ProcessDefinitionRepository extends ActivitiBaseRepository<ActProcessDefinition, String> {

}
