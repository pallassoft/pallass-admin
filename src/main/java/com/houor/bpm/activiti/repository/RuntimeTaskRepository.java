package com.houor.bpm.activiti.repository;

import org.springframework.stereotype.Repository;

import com.houor.bpm.activiti.domain.ActRuntimeTask;
import com.houor.common.repository.ActivitiBaseRepository;

@Repository
public interface RuntimeTaskRepository extends ActivitiBaseRepository<ActRuntimeTask, String> {

}
