package com.houor.bpm.activiti.repository;

import org.springframework.stereotype.Repository;

import com.houor.bpm.activiti.domain.ActHistoryTask;
import com.houor.common.repository.ActivitiBaseRepository;

@Repository
public interface HistoryTaskRepository extends ActivitiBaseRepository<ActHistoryTask, String> {

}
