package com.houor.common.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import com.houor.common.domain.BaseDomain;
import com.houor.common.repository.BaseRepository;

@Transactional
public abstract class BaseServiceImpl<T extends BaseDomain, ID extends java.io.Serializable> implements BaseService<T, ID> {

	public abstract BaseRepository<T, ID> getRepository();

	@Override
	public T findOne(ID id) {
		return getRepository().findOne(id);
	}

	@Override
	public List<T> findAll() {
		return getRepository().findAll();
	}

	@Override
	public List<T> findAll(ID[] ids) {
		List<ID> idList = Arrays.asList(ids);
		return getRepository().findAll(idList);
	}

	@Override
	public List<T> findAll(Specification<T> spec, Sort sort) {
		return getRepository().findAll(spec, sort);
	}

	@Override
	public Page<T> findAll(Pageable pageable) {
		return getRepository().findAll(pageable);
	}

	@Override
	public List<T> findAll(Iterable<ID> ids) {
		return getRepository().findAll(ids);
	}

	@Override
	public Page<T> findAll(Specification<T> spec, Pageable pageable) {
		return getRepository().findAll(spec, pageable);
	}

	@Override
	public long count() {
		return getRepository().count();
	}

	@Override
	public long count(Specification<T> spec) {
		return getRepository().count(spec);
	}

	@Override
	public boolean exists(ID id) {
		return getRepository().exists(id);
	}

	@Override
	public T save(T entity) {
		entity.setCreateTime(new Date());
		entity.setUpdateTime(new Date());
		return getRepository().save(entity);
	}

	public void save(Iterable<T> entitys) {
		for (T entity : entitys) {
			entity.setCreateTime(new Date());
			entity.setUpdateTime(new Date());
		}
		getRepository().save(entitys);
	}

	@Override
	public T saveAndFlush(T entity) {
		// set createTime for new object
		if (entity.getCreateTime() == null) {
			entity.setCreateTime(new Date());
		}
		entity.setUpdateTime(new Date());
		return getRepository().saveAndFlush(entity);
	}

	@Override
	public void delete(ID id) {
		getRepository().delete(id);
	}

	@Override
	public void deleteByIds(@SuppressWarnings("unchecked") ID... ids) {
		if (ids != null) {
			for (int i = 0; i < ids.length; i++) {
				ID id = ids[i];
				this.delete(id);
			}
		}
	}

	public void deleteByIdArray(ID ids[]) {
		if (ids != null) {
			for (ID id : ids) {
				this.delete(id);
			}
		}
	}

	@Override
	public void delete(T[] entitys) {
		List<T> tList = Arrays.asList(entitys);
		getRepository().delete(tList);
	}

	@Override
	public void delete(Iterable<T> entitys) {
		getRepository().delete(entitys);
	}

	@Override
	public void delete(T entity) {
		getRepository().delete(entity);
	}

	@Override
	public T update(T entity) {
		return getRepository().saveAndFlush(entity);
	}

}
