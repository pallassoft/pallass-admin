package com.houor.common.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

public interface BaseService<T, ID extends java.io.Serializable> {

	public T findOne(ID id);

	public List<T> findAll();

	public List<T> findAll(ID[] ids);

	public List<T> findAll(Iterable<ID> ids);

	public Page<T> findAll(Pageable pageable);

	public Page<T> findAll(Specification<T> spec, Pageable pageable);

	public long count();

	public long count(Specification<T> spec);

	public boolean exists(ID id);

	public T save(T entity);

	public T saveAndFlush(T entity);

	public void delete(ID id);

	public void deleteByIds(@SuppressWarnings("unchecked") ID... ids);

	public void deleteByIdArray(ID ids[]);

	public void delete(T[] entitys);

	public void delete(Iterable<T> entitys);

	public void delete(T entity);

	public List<T> findAll(Specification<T> spec, Sort sort);

	public T update(T entity);

}
