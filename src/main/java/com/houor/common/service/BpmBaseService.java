package com.houor.common.service;

/**
 * 业务流程服务实现基础接口
 * 
 * @author Houor
 *
 * @createTime 2017-08-16 18:07
 */
public interface BpmBaseService<T, ID extends java.io.Serializable> extends BaseService<T, ID> {

	/**
	 * 启动业务流程并保存业务对象
	 * 
	 * @param domain
	 * @return
	 */
	T saveAndStartProcess(T domain);

	/**
	 * 启动业务流程任务或办理任务
	 * 
	 * @param entity
	 * @return
	 */
	T startOrTodoTask(T domain);

	/**
	 * 获取并设置任务信息
	 * 
	 * @param entity
	 * 
	 * @return
	 */
	int handleTaskInfo(T domain);

}
