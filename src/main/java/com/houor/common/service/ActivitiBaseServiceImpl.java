package com.houor.common.service;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import com.houor.common.domain.ActivitiBaseDomain;
import com.houor.common.repository.ActivitiBaseRepository;

/**
 * Activiti服务实现基础类
 * 
 * @author Houor
 *
 * @createTime 2017-08-16 18:08
 */
@Transactional
public abstract class ActivitiBaseServiceImpl<T extends ActivitiBaseDomain, ID extends Serializable> implements BaseService<T, ID> {

	public abstract ActivitiBaseRepository<T, ID> getBaseRepository();

	@Override
	public T findOne(ID id) {
		return getBaseRepository().findOne(id);
	}

	@Override
	public List<T> findAll() {
		return getBaseRepository().findAll();
	}

	@Override
	public List<T> findAll(ID[] ids) {
		List<ID> idList = Arrays.asList(ids);
		return getBaseRepository().findAll(idList);
	}

	@Override
	public List<T> findAll(Specification<T> spec, Sort sort) {
		return getBaseRepository().findAll(spec, sort);
	}

	@Override
	public Page<T> findAll(Pageable pageable) {
		return getBaseRepository().findAll(pageable);
	}

	@Override
	public List<T> findAll(Iterable<ID> ids) {
		return getBaseRepository().findAll(ids);
	}

	@Override
	public Page<T> findAll(Specification<T> spec, Pageable pageable) {
		return getBaseRepository().findAll(spec, pageable);
	}

	@Override
	public long count() {
		return getBaseRepository().count();
	}

	@Override
	public long count(Specification<T> spec) {
		return getBaseRepository().count(spec);
	}

	@Override
	public boolean exists(ID id) {
		return getBaseRepository().exists(id);
	}

	@Override
	public T save(T entity) {
		return getBaseRepository().save(entity);
	}

	public void save(Iterable<T> entitys) {

		getBaseRepository().save(entitys);
	}

	@Override
	public T saveAndFlush(T entity) {
		return getBaseRepository().saveAndFlush(entity);
	}

	@Override
	public void delete(ID id) {
		getBaseRepository().delete(id);
	}

	@Override
	public void deleteByIds(@SuppressWarnings("unchecked") ID... ids) {
		if (ids != null) {
			for (int i = 0; i < ids.length; i++) {
				ID id = ids[i];
				this.delete(id);
			}
		}
	}

	public void deleteByIdArray(ID ids[]) {
		if (ids != null) {
			for (ID id : ids) {
				this.delete(id);
			}
		}
	}

	@Override
	public void delete(T[] entitys) {
		List<T> tList = Arrays.asList(entitys);
		getBaseRepository().delete(tList);
	}

	@Override
	public void delete(Iterable<T> entitys) {
		getBaseRepository().delete(entitys);
	}

	@Override
	public void delete(T entity) {
		getBaseRepository().delete(entity);
	}

	@Override
	public T update(T entity) {
		return getBaseRepository().saveAndFlush(entity);
	}

}
