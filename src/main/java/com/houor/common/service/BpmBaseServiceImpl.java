package com.houor.common.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.FormService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.houor.common.domain.BpmBaseDomain;
import com.houor.common.utils.BpmTaskUtils;
import com.pallass.security.util.SecurityUtil;

/**
 * 业务流程服务实现基础类(Activiti)
 * 
 * @author Houor
 *
 * @createTime 2017-08-16 18:07
 */
@Transactional
public abstract class BpmBaseServiceImpl<T extends BpmBaseDomain, ID extends java.io.Serializable> extends BaseServiceImpl<T, ID> implements BpmBaseService<T, ID> {

	private static Logger logger = LoggerFactory.getLogger(BpmBaseServiceImpl.class);

	@Autowired
	RuntimeService runtimeService;

	@Autowired
	IdentityService identityService;

	@Autowired
	TaskService taskService;

	@Autowired
	FormService formService;

	@Autowired
	BpmTaskUtils bpmTaskUtils;

	@Override
	public T saveAndStartProcess(T entity) {

		// 保存数据
		entity.setCreateTime(new Date());
		entity.setUpdateTime(new Date());
		getRepository().save(entity);

		// 启动流程
		String userId = SecurityUtil.getCurrentUserLoginName();
		ProcessInstance pi = bpmTaskUtils.startProcess(userId, entity.getProcessDefinitionId(), entity, null);
		entity.setProcessInstanceId(pi.getId());
		getRepository().saveAndFlush(entity);

		return entity;

	}

	@Override
	public T startOrTodoTask(T entity) {
		// 启动流程
		if (entity.getStatus().equals("start")) {
			return saveAndStartProcess(entity);
		}
		// 办理任务
		else if (entity.getStatus().equals("todo")) {
			Map<String, Object> variables = new HashMap<>();
			// 如果业务对象有对应的审批意见, 则设置审批意见
			try {
				String methodName = "set" + entity.getAuditValueName().substring(0, 1).toUpperCase() + entity.getAuditValueName().substring(1);
				Method setMethod = entity.getClass().getMethod(methodName, String.class);
				setMethod.invoke(entity, entity.getAuditValue());
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				logger.debug("Entity has no audit property.");
			}
			saveAndFlush(entity);

			variables.put(entity.getAuditValueName(), entity.getAuditValue());

			bpmTaskUtils.complete(entity.getTaskId(), entity.getProcessInstanceId(), entity.getComment(), variables);
		}

		return entity;
	}

	@Override
	public int handleTaskInfo(T domain) {
		return bpmTaskUtils.handleTaskInfo(domain);
	}

}
