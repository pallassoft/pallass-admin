package com.houor.common.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import com.houor.common.domain.ActivitiBaseDomain;

/**
 * Activiti Repository for Generated Activiti Object
 * 
 * @author Houor
 *
 * @createTime 2017-08-16 21:05
 */
@NoRepositoryBean
public interface ActivitiBaseRepository<T extends ActivitiBaseDomain, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

}
