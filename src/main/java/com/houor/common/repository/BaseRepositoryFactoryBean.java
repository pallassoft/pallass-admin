package com.houor.common.repository;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import com.houor.common.domain.BaseDomain;

public class BaseRepositoryFactoryBean<REPO extends JpaRepository<T, ID>, T, ID extends java.io.Serializable> extends JpaRepositoryFactoryBean<REPO, T, ID> {

	public BaseRepositoryFactoryBean(Class<? extends REPO> repositoryInterface) {
		super(repositoryInterface);
	}

	@SuppressWarnings("rawtypes")
	protected RepositoryFactorySupport createRepositoryFactory(EntityManager em) {
		return new BaseRepositoryFactory(em);
	}

}

class BaseRepositoryFactory<T extends BaseDomain, I extends Serializable> extends JpaRepositoryFactory {

	private final EntityManager em;

	public BaseRepositoryFactory(EntityManager em) {
		super(em);
		this.em = em;
	}

	@SuppressWarnings("unchecked")
	protected Object getTargetRepository(RepositoryMetadata metadata) {
		return new BaseRepositoryImpl<T, I>((Class<T>) metadata.getDomainType(), em);
	}

	protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
		return BaseRepositoryImpl.class;
	}
}
