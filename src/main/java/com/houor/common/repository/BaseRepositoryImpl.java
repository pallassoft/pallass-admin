package com.houor.common.repository;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.houor.common.domain.BaseDomain;

/**
 * the implementation of base repository
 * 
 * extends SimpleJpaRepository for adding some useful methods
 * 
 * @author Houor
 *
 * @createTime 2018-01-28 21:34
 */
public class BaseRepositoryImpl<T extends BaseDomain, ID extends Serializable> extends SimpleJpaRepository<T, Serializable> implements BaseRepository<T, Serializable> {

	private final EntityManager em;

	public BaseRepositoryImpl(Class<T> domainClass, EntityManager em) {
		super(domainClass, em);
		this.em = em;
	}

	public BaseRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
		super(entityInformation, entityManager);
		this.em = entityManager;
	}

	@Override
	@Transactional
	public T update(T entity) {
		T target = em.find(getDomainClass(), entity.getId());
		Assert.notNull(target, "The Object for updating can not be null.");

		BeanUtils.copyProperties(entity, target, getNullPropertyNames(entity));

		return em.merge(target);
	}

	/**
	 * get array of property names where their value is null
	 * 
	 * @param source
	 * @return
	 */
	private static String[] getNullPropertyNames(Object source) {
		BeanWrapper sourceBean = new BeanWrapperImpl(source);
		PropertyDescriptor[] pds = sourceBean.getPropertyDescriptors();
		Set<String> emptyName = new HashSet<>();
		for (PropertyDescriptor pd : pds) {
			Object sourceValue = sourceBean.getPropertyValue(pd.getName());
			if (sourceValue == null)
				emptyName.add(pd.getName());
		}

		String[] result = new String[emptyName.size()];
		return emptyName.toArray(result);
	}

}
