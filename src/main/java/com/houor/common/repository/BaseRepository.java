package com.houor.common.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import com.houor.common.domain.BaseDomain;

/**
 * Custom Repository
 * 
 * @author Houor
 *
 * @createTime 2018-01-24 21:22
 */
@NoRepositoryBean
public interface BaseRepository<T extends BaseDomain, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

	/**
	 * update properties where entity's properties is not null
	 * 
	 * @param entity
	 * @return
	 */
	public T update(T entity);
}