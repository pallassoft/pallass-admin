package com.houor.common.repository;

import java.io.Serializable;

import org.springframework.data.repository.NoRepositoryBean;

import com.houor.common.domain.BpmBaseDomain;

/**
 * Bpm Repository for business object
 * 
 * @author Houor
 *
 * @createTime 2017-08-16 21:07
 */
@NoRepositoryBean
public interface BpmBaseRepository<T extends BpmBaseDomain, ID extends Serializable> extends BaseRepository<T, ID> {

}
