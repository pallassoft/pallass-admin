package com.houor.common.utils;

import java.util.Map;

import org.activiti.engine.runtime.ProcessInstance;

import com.houor.common.domain.BpmBaseDomain;

/**
 * 
 * 业务流程任务工具类
 * 
 * @author Houor
 *
 * @createTime 2017-08-18 11:31
 */
public interface BpmTaskUtils {

	/**
	 * 根据流程定义ID启动流程
	 * 
	 * @param processDefinitionId
	 * @return
	 */
	public ProcessInstance startProcess(String processDefinitionId);

	/**
	 * 启动流程
	 * 
	 * @param userId
	 * @param processDefinitionId
	 * @param businessObject
	 * @param values
	 * @return
	 */
	ProcessInstance startProcess(String userId, String processDefinitionId, BpmBaseDomain businessObject, Map<String, Object> values);

	/**
	 * 获取流程开始表单
	 * 
	 * @param processDefinitionId
	 * @return
	 */
	public String getStartFormKey(String processDefinitionId);

	/**
	 * 获取流程任务表单
	 * 
	 * @param processDefinitionId
	 * @param taskDefinitionKey
	 * @return
	 */
	public String getTaskFormKey(String processDefinitionId, String taskDefinitionKey);

	/**
	 * 获取流程任务表单
	 * 
	 * @param taskId
	 * @return
	 */
	public String getTaskFormKey(String taskId);

	/**
	 * 获取流程任务表单
	 * 
	 * @param taskId
	 * @param urlProperties
	 * @return
	 */
	public String getTaskFormKey(String taskId, Map<String, String> urlProperties);

	/**
	 * 签收任务
	 * 
	 * @param taskIds
	 * @param userId
	 * @return
	 */
	public String claimTask(String[] taskIds, String userId);

	/**
	 * 获取并设置任务信息
	 * 
	 * @param domain
	 */
	public int handleTaskInfo(BpmBaseDomain domain);

	/**
	 * 完成任务
	 * 
	 * @param taskId
	 * @param processInstanceId
	 * @param comment
	 * @param variables
	 */
	void complete(String taskId, String processInstanceId, String comment, Map<String, Object> variables);

}
