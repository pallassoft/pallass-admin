package com.houor.common.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Activiti领域模型基类
 * 
 * @author Houor
 *
 * @createTime: 2017-08-13 17:19
 */
@MappedSuperclass
public class ActivitiBaseDomain implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 470742656376305044L;

	@Id
	@Column(name = "ID_", nullable = false)
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
