package com.houor.common.domain;

import java.util.Map;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 * 业务流程管理领域基类
 * 
 * @author Houor
 *
 * @createTime 2017-08-16 17:08
 */
@MappedSuperclass
public abstract class BpmBaseDomain extends BaseDomain implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4350195534846012661L;

	private String processDefinitionId;

	private String processInstanceId;

	@Transient
	private String taskId;

	@Transient
	private String taskDefinitionKey;

	private String status;// 执行状态(start, todo, claim, finish)

	@Transient
	private String auditValueName;// 当前任务审批变量名

	@Transient
	private String auditValue;// 当前任务审批变量值

	@Transient
	private Map<String, String> transitionConditionMap;// 任务走向条件Enum

	@Transient
	private String comment;// 审批意见

	/**
	 * clone领域类任务信息
	 * 
	 * @param domain
	 */
	public void cloneTaskInfo(BpmBaseDomain domain) {
		this.setTaskId(domain.getTaskId());
		this.setTaskDefinitionKey(domain.getTaskDefinitionKey());
		this.setAuditValueName(domain.getAuditValueName());
		this.setStatus(domain.getStatus());
		this.setTransitionConditionMap(domain.getTransitionConditionMap());
		this.setComment(domain.getComment());
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}

	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAuditValueName() {
		return auditValueName;
	}

	public void setAuditValueName(String auditValueName) {
		this.auditValueName = auditValueName;
	}

	public Map<String, String> getTransitionConditionMap() {
		return transitionConditionMap;
	}

	public void setTransitionConditionMap(Map<String, String> transitionConditionMap) {
		this.transitionConditionMap = transitionConditionMap;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getAuditValue() {
		return auditValue;
	}

	public void setAuditValue(String auditValue) {
		this.auditValue = auditValue;
	}

}
