package com.houor.common.controller;

import org.springframework.beans.factory.annotation.Autowired;

import com.houor.common.domain.BpmBaseDomain;
import com.houor.common.service.BpmBaseService;
import com.houor.common.utils.BpmTaskUtils;

public class BpmBaseController<T extends BpmBaseDomain> extends BaseController {

	@Autowired
	private BpmTaskUtils bpmTaskUtils;

	public BpmBaseService<T, Integer> getBpmBaseService() {
		return null;
	}

	protected T handleTaskInfo(T domain) {
		// 设置任务信息
		bpmTaskUtils.handleTaskInfo(domain);

		T entity = this.getBpmBaseService().findOne(domain.getId());
		entity.cloneTaskInfo(domain);
		return entity;
	}

}
