package com.houor.common.data.jpa.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import com.houor.common.data.jpa.criteria.CompositionType;
import com.houor.common.data.jpa.criteria.OperatorType;

/**
 * 查询列表
 * 
 * @author Houor
 *
 * @createTime: 2017-02-20 20:36
 * 
 */
public class ListSpecificationBuilder<T> {

	private List<SpecificationItem> itemList;

	public ListSpecificationBuilder() {
		itemList = new ArrayList<>();
	}
	
	public ListSpecificationBuilder<T> add(String subClassProperty, String key, OperatorType operatorType, Object value) {
		return add(subClassProperty, key, operatorType, value, CompositionType.AND);
	}

	public ListSpecificationBuilder<T> add(String key, OperatorType operatorType, Object value) {
		return add(key, operatorType, value, CompositionType.AND);
	}

	public ListSpecificationBuilder<T> or(String key, OperatorType operatorType, Object value) {
		return add(key, operatorType, value, CompositionType.OR);
	}

	public Specification<T> generateListSpecification() {
		return new ListSpecification<T>(itemList);
	}

	private ListSpecificationBuilder<T> add(String key, OperatorType operatorType, Object value,
			CompositionType compositionType) {
		SpecificationItem item = new SpecificationItem(key, operatorType, value, compositionType);
		itemList.add(item);
		return this;
	}

	private ListSpecificationBuilder<T> add(String subClassProperty, String key, OperatorType operatorType, Object value, CompositionType compositionType) {
		SpecificationItem item = new SpecificationItem(subClassProperty, key, operatorType, value, compositionType);
		itemList.add(item);
		return this;
	}

	

}
