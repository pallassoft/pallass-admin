package com.houor.common.data.jpa.domain;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.houor.common.data.jpa.criteria.CompositionType;
import com.houor.common.data.jpa.criteria.OperatorType;

/**
 * Specification条目
 * 
 * 记录每条Specification的键值对、操作符和连接方式
 * 
 * @author Houor
 * 
 * @createTime: 2017-02-20 23:36
 *
 */
public class SpecificationItem {

	/**
	 * 查询属性
	 */
	private String key;

	/**
	 * 查询值
	 */
	private Object value;

	private String subClassProperty;

	/**
	 * 操作符
	 * 
	 * @see com.houor.common.data.jpa.criteria.OperatorType
	 */
	private OperatorType operatorType;

	/**
	 * 连接方式: AND, OR
	 * 
	 * @see com.houor.common.data.jpa.criteria.CompositionType
	 */
	private CompositionType compositionType;

	public SpecificationItem(String key, OperatorType operatorType, Object value, CompositionType compositionType) {
		super();
		this.key = key;
		this.operatorType = operatorType;
		this.value = value;
		this.compositionType = compositionType;
	}

	public SpecificationItem(String subClassProperty, String key, OperatorType operatorType, Object value,
			CompositionType compositionType) {
		super();
		this.subClassProperty = subClassProperty;
		this.key = key;
		this.operatorType = operatorType;
		this.value = value;
		this.compositionType = compositionType;
	}

	public Predicate generateOperatorPredicate(Root<?> root, CriteriaBuilder criteriaBuilder) {
		return operatorType.combine(root, criteriaBuilder, this);
	}

	public Predicate generateCompositionPredicate(CriteriaBuilder criteriaBuilder, Predicate lhs, Predicate rhs) {
		return compositionType.combine(criteriaBuilder, lhs, rhs);
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public OperatorType getOperatorType() {
		return operatorType;
	}

	public void setOperatorType(OperatorType operatorType) {
		this.operatorType = operatorType;
	}

	public CompositionType getCompositionType() {
		return compositionType;
	}

	public void setCompositionType(CompositionType compositionType) {
		this.compositionType = compositionType;
	}

	public String getSubClassProperty() {
		return subClassProperty;
	}

	public void setSubClassProperty(String subClassProperty) {
		this.subClassProperty = subClassProperty;
	}

}
