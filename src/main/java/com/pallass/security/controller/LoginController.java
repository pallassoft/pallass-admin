package com.pallass.security.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pallass.admin.domain.SysUser;
import com.pallass.admin.dto.SysMenu;
import com.pallass.admin.service.SysResourceService;
import com.pallass.admin.service.SysUserService;
import com.pallass.security.CustomerUser;
import com.pallass.security.util.BCryptUtil;
import com.pallass.security.util.SecurityUtil;
import com.pallass.utils.RequestResult;

@Controller
public class LoginController {

	@Autowired
	private SysUserService userService;

	@Autowired
	private SysResourceService resourceService;

	@RequestMapping(value = { "/index", "/" }, method = RequestMethod.GET)
	public String index(Model model) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null) {
			return "login";
		}

		if (auth instanceof AnonymousAuthenticationToken) {
			return "login";
		}

		CustomerUser principal = (CustomerUser) auth.getPrincipal();

		List<SysMenu> menus = resourceService.obtainMenus(principal.getId());

		model.addAttribute("menus", menus);

		return "index";
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String welcome() {
		return "welcome";
	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String loginPage(@RequestParam(value = "error", required = false) String error, Model model) {
		if (error != null) {
			model.addAttribute("error", "error");
			return "login";
		}
		return "login";
	}

	@RequestMapping(value = { "/accessDenied" })
	public String accessDenied() {
		return "accessDenied";
	}

	@RequestMapping(value = "/sec/passwd", method = RequestMethod.POST)
	@ResponseBody
	public RequestResult passwd(@RequestBody PasswordData passwordData) {

		if (StringUtils.isBlank(passwordData.getPassword())) {
			return RequestResult.failure("原有密码不为能空");
		}

		if (StringUtils.isBlank(passwordData.getNewPassword())) {
			return RequestResult.failure("新密码不为能空");
		}

		CustomerUser user = SecurityUtil.getCurrentUser();

		if (user == null) {
			return RequestResult.failure("用户未登录或会话已失效, 请重新登录");
		}

		// FIXME when the user is a teacher or student, system should update the
		// password of corresponding module
		SysUser su = userService.findOne(user.getId());

		if (!BCryptUtil.compare(passwordData.getPassword(), su.getPassword())) {
			return RequestResult.failure("原密码输入错误");
		}

		String newSecurityPwd = BCryptUtil.generateBcryptPasswd(passwordData.getNewPassword());

		su.setPassword(newSecurityPwd);
		userService.update(su);

		SecurityUtil.logout();

		return RequestResult.success("密码修改成功");
	}

}

class PasswordData implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2842280340535298545L;
	String password;
	String newPassword;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

}
