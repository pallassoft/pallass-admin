package com.pallass.security.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.pallass.security.CustomerUser;

public class SecurityUtil {

	public static CustomerUser getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null)
			return null;
		return auth.getPrincipal() == null ? null : (CustomerUser) auth.getPrincipal();
	}

	public static String getCurrentUserLoginName() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null)
			return null;
		return auth.getPrincipal() == null ? "" : ((CustomerUser) auth.getPrincipal()).getUsername();
	}

	public static void logout() {
		SecurityContextHolder.clearContext();
	}

}
