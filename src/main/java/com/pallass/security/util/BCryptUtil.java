package com.pallass.security.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptUtil {

	private static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	public static String generateBcryptPasswd(String input) {
		return encoder.encode(input);
	}

	public static boolean compare(String plainText, String encodePwd) {
		return encoder.matches(plainText, encodePwd);
	}

}
