package com.pallass.admin.service;

import com.houor.common.service.BaseService;
import com.pallass.admin.domain.SysRole;

public interface SysRoleService extends BaseService<SysRole, Integer> {

	void grant(Integer roleId, Integer[] resourceIds);

}
