package com.pallass.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.houor.common.repository.BaseRepository;
import com.houor.common.service.BaseServiceImpl;
import com.pallass.admin.domain.SysResource;
import com.pallass.admin.dto.SysMenu;
import com.pallass.admin.factory.SysMenuFactory;
import com.pallass.admin.repository.SysResourceRepository;
import com.pallass.admin.service.SysResourceService;

@Service("resourceService")
public class SysResourceServiceImpl extends BaseServiceImpl<SysResource, Integer> implements SysResourceService {

	@Autowired
	private SysResourceRepository sysResourceDAO;

	public BaseRepository<SysResource, Integer> getRepository() {
		return this.sysResourceDAO;
	}

	public List<SysResource> findAllWithoutButton() {
		return sysResourceDAO.findAllWithoutButton();
	}

	@Override
	public List<SysResource> obtainResources(Integer userId) {
		return sysResourceDAO.findMenuByUserId(userId);
	}

	@Override
	public List<String> findPermissionsByUserId(Integer userId) {
		return sysResourceDAO.findPermissionsByUserId(userId);
	}

	@Override
	public List<SysMenu> obtainMenus(Integer userId) {
		List<SysResource> resources = sysResourceDAO.findMenuByUserId(userId);

		return SysMenuFactory.generateMenuTree(resources);

	}

}
