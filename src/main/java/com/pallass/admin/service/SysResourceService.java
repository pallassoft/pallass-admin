package com.pallass.admin.service;

import java.util.List;

import com.houor.common.service.BaseService;
import com.pallass.admin.domain.SysResource;
import com.pallass.admin.dto.SysMenu;

public interface SysResourceService extends BaseService<SysResource, Integer> {

	List<SysResource> findAllWithoutButton();

	List<SysResource> obtainResources(Integer userId);

	List<String> findPermissionsByUserId(Integer userId);

	List<SysMenu> obtainMenus(Integer userId);

}
