package com.pallass.admin.service;

import java.util.List;

import com.houor.common.service.BaseService;
import com.pallass.admin.domain.SysUser;

public interface SysUserService extends BaseService<SysUser, Integer> {

	void grant(Integer id, Integer[] roleIds);

	SysUser uniqueUserByUserName(String userName);

	void resetpw(Integer id);

	List<String> getRoleKeyByUserName(String userName);

}
