package com.pallass.admin.factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pallass.admin.domain.SysResource;
import com.pallass.admin.dto.SysMenu;

public class SysMenuFactory {

	public static List<SysMenu> generateMenuTree(List<SysResource> resources) {
		Map<Integer, SysMenu> map = new HashMap<>();
		List<SysMenu> menus = new ArrayList<>();

		for (SysResource resource : resources) {
			if (resource.getParent() == null) {
				map.put(resource.getId(), generateMenu(resource));
			}
		}

		for (SysResource resource : resources) {
			if (resource.getParent() != null) {

				SysMenu menu = map.get(resource.getParent().getId());
				menu.getChildren().add(generateMenu(resource));
			}
		}

		for (SysMenu menu : map.values()) {
			menus.add(menu);
		}

		return menus;
	}

	public static SysMenu generateMenu(SysResource resource) {

		SysMenu menu = new SysMenu();
		menu.setId(resource.getId());
		if (resource.getParent() != null) {
			menu.setParentId(resource.getParent().getId());
		}

		menu.setChildren(new ArrayList<SysMenu>());

		menu.setResourceName(resource.getResourceName());
		menu.setResourceUrl(resource.getResourceUrl());
		menu.setResourceType(resource.getResourceType());
		menu.setLevel(resource.getLevel());
		menu.setOrderNum(resource.getOrderNum());
		menu.setIcon(resource.getIcon());

		return menu;

	}

}
