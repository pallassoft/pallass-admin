package com.pallass.admin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.houor.common.repository.BaseRepository;
import com.pallass.admin.domain.SysRole;
import com.pallass.admin.domain.SysUser;

@Repository
public interface SysUserRepository extends BaseRepository<SysUser, Integer> {

	public List<SysUser> findByUserName(String userName);

	@Query("select u.roles from SysUser u where u.id = ?1")
	public List<SysRole> findRolesByUserId(Integer userId);

	@Query("select u.roles from SysUser u where u.userName = ?1")
	public List<SysRole> findRolesByUserName(String userName);

}
