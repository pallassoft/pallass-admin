package com.pallass.admin.repository;

import org.springframework.stereotype.Repository;

import com.houor.common.repository.BaseRepository;
import com.pallass.admin.domain.SysRole;

@Repository
public interface SysRoleRepository extends BaseRepository<SysRole, Integer> {

}
