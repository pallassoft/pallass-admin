package com.pallass.admin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.houor.common.repository.BaseRepository;
import com.pallass.admin.domain.SysResource;

@Repository
public interface SysResourceRepository extends BaseRepository<SysResource, Integer> {

	@Query("from SysResource sr where sr.resourceType != 2")
	List<SysResource> findAllWithoutButton();

	@Query(value = "SELECT distinct permissions FROM sys_resource res inner join sys_role_resource srr on res.id = srr.resource_id inner join sys_role srole on srr.role_id = srole.id inner join sys_user_role sur on sur.role_id = srole.id where sur.user_id = ?1", nativeQuery = true)
	List<String> findPermissionsByUserId(Integer userId);
	
	@Query(value = "SELECT distinct res.* FROM sys_resource res inner join sys_role_resource srr on res.id = srr.resource_id inner join sys_role srole on srr.role_id = srole.id inner join sys_user_role sur on sur.role_id = srole.id where sur.user_id = ?1 and res.resourceType != 2", nativeQuery = true)
	List<SysResource> findMenuByUserId(Integer userId);

}
