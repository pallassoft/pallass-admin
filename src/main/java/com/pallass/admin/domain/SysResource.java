package com.pallass.admin.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.houor.common.domain.BaseDomain;
import com.pallass.common.vo.ZtreeObject;

/**
 * 系统资源
 * 
 * @author Houor
 *
 * @createTime: 2017-02-07 23:36
 */
@Entity
@Table(name = "sys_resource")
public class SysResource extends BaseDomain implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6852562414193636681L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", foreignKey = @ForeignKey(name = "fk_sys_resource_parent"))
	private SysResource parent;

	@Transient
	private List<SysResource> children = new ArrayList<SysResource>();

	/**
	 * 父菜单ID，一级菜单为0, 由上级对象parent生产
	 */
	@Transient
	private Integer parentId;

	/**
	 * 父菜单名称, 由上级对象parent获得
	 */
	@Transient
	private String parentName;

	/**
	 * 资源名称
	 */
	private String resourceName;

	/**
	 * 资源唯一标识
	 */
	private String resourceKey;

	/**
	 * 资源url
	 */
	private String resourceUrl;

	/**
	 * 资源类型 --> 0:目录;1:菜单;2:按钮
	 */
	private Integer resourceType;

	/**
	 * 所需授权(多个用逗号分隔，如：user_list,user_create)
	 */
	private String permissions;

	/**
	 * 层级
	 */
	@Column(name = "resourceLevel")
	private Integer level;

	/**
	 * 排序
	 */
	private Integer orderNum;

	/**
	 * 图标
	 */
	private String icon;

	/**
	 * 是否隐藏
	 * 
	 * 0显示 1隐藏
	 */
	private Integer isHide;

	@Transient
	private boolean open;

	@Transient
	private boolean checked = false;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getResourceKey() {
		return resourceKey;
	}

	public void setResourceKey(String resourceKey) {
		this.resourceKey = resourceKey;
	}

	public String getResourceUrl() {
		return resourceUrl;
	}

	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

	public Integer getResourceType() {
		return resourceType;
	}

	public void setResourceType(Integer resourceType) {
		this.resourceType = resourceType;
	}

	public String getPermissions() {
		return permissions;
	}

	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getIsHide() {
		return isHide;
	}

	public void setIsHide(Integer isHide) {
		this.isHide = isHide;
	}

	public SysResource getParent() {
		return parent;
	}

	public void setParent(SysResource parent) {
		this.parent = parent;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public Integer getParentId() {
		return parent != null ? parent.getId() : 0;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parent != null ? parent.getId() : 0;
	}

	public String getParentName() {
		return parent != null ? parent.getResourceName() : "根资源节点";
	}

	public void setParentName(String parentName) {
		this.parentName = parent != null ? parent.getResourceName() : "根资源节点";
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public ZtreeObject generateZtreeObject() {
		return new ZtreeObject(this.getId(), this.getParentId(), this.getResourceName(), true, false);
	}

	public List<SysResource> getChildren() {
		return children;
	}

	public void setChildren(List<SysResource> children) {
		this.children = children;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

}
