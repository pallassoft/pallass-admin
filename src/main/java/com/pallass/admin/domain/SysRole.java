package com.pallass.admin.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.houor.common.domain.BaseDomain;

/**
 * 系统角色
 * 
 * @author Houor
 *
 * @createTime: 2017-02-07 23:46
 */
@Entity
@Table(name = "sys_role")
public class SysRole extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -527096910940391580L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Integer id;

	/**
	 * 角色名称
	 */
	private String roleName;

	/**
	 * 角色标识
	 */
	private String roleKey;

	/**
	 * 角色状态: 0：正常；1：删除
	 */
	private Integer status;

	@Transient
	private boolean checked = false;

	@ManyToMany(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinTable(name = "sys_role_resource", joinColumns = { @JoinColumn(name = "role_id", foreignKey = @ForeignKey(name = "fk_role_resource_role_id")) }, inverseJoinColumns = {
			@JoinColumn(name = "resource_id", foreignKey = @ForeignKey(name = "fk_role_resource_resource_id")) })
	private java.util.Set<SysResource> resources;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleKey() {
		return roleKey;
	}

	public void setRoleKey(String roleKey) {
		this.roleKey = roleKey;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public java.util.Set<SysResource> getResources() {
		return resources;
	}

	public void setResources(java.util.Set<SysResource> resources) {
		this.resources = resources;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

}
