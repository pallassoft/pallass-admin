package com.pallass.admin.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pallass.common.domain.Person;
import com.pallass.organization.domain.Department;

/**
 * 系统用户
 * 
 * @author Houor
 *
 * @createTime: 2017-02-07 23:39
 */
@Entity
@Table(name = "sys_user")
public class SysUser extends Person {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7350562280572514741L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_dept", foreignKey = @ForeignKey(name = "fk_user_department_id"))
	private Department department;

	@ManyToMany(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinTable(name = "sys_user_role", joinColumns = { @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "fk_user_role_user_id")) }, inverseJoinColumns = {
			@JoinColumn(name = "role_id", foreignKey = @ForeignKey(name = "fk_user_role_role_id")) })
	private java.util.Set<SysRole> roles;

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public java.util.Set<SysRole> getRoles() {
		return roles;
	}

	public void setRoles(java.util.Set<SysRole> roles) {
		this.roles = roles;
	}

}
