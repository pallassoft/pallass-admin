package com.pallass.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.header.HeaderWriter;
import org.springframework.security.web.header.HeaderWriterFilter;

import com.houor.spring.security.access.vote.UrlAccessDecisionManager;
import com.houor.spring.security.web.intercept.UrlFilterInvocationSecurityMetadataSource;
import com.pallass.admin.domain.SysResource;
import com.pallass.security.service.CustomerUserDetailsService;

/**
 * 系统安全配置
 * 
 * @author Houor
 *
 * @createTime: 2017-02-27 10:46
 */
@Configuration
@EnableWebSecurity
public class PallassSecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static Logger logger = LoggerFactory.getLogger(PallassSecurityConfiguration.class);

	@Autowired
	CustomerUserDetailsService userDetailsService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		logger.debug("configure http security start......");

		http.formLogin().loginPage("/login").defaultSuccessUrl("/index").and().logout().logoutSuccessUrl("/login").and().authorizeRequests().withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {

			@Override
			public <O extends FilterSecurityInterceptor> O postProcess(O object) {

				object.setSecurityMetadataSource(new UrlFilterInvocationSecurityMetadataSource(getUrlPermissonMap()));
				object.setAccessDecisionManager(new UrlAccessDecisionManager());

				return object;
			}
		}).antMatchers("/index").authenticated().and().exceptionHandling().accessDeniedPage("/accessDenied");

		http.rememberMe().tokenValiditySeconds(60);
		// http.sessionManagement().sessionFixation().changeSessionId().maximumSessions(1).maxSessionsPreventsLogin(true).expiredUrl("/login");
		http.sessionManagement().invalidSessionUrl("/login").maximumSessions(1);

		http.csrf().disable();

		http.headers().disable();
		HeaderWriter headerWriter = new HeaderWriter() {
			public void writeHeaders(HttpServletRequest request, HttpServletResponse response) {
				response.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
				response.setHeader("Expires", "0");
				response.setHeader("Pragma", "no-cache");
				response.setHeader("X-Frame-Options", "SAMEORIGIN");
				response.setHeader("X-XSS-Protection", "1; mode=block");
				response.setHeader("x-content-type-options", "nosniff");
			}
		};
		List<HeaderWriter> headerWriterFilterList = new ArrayList<>();
		headerWriterFilterList.add(headerWriter);
		HeaderWriterFilter headerFilter = new HeaderWriterFilter(headerWriterFilterList);
		http.addFilter(headerFilter);

		logger.debug("configure http security end.");
	}

	@Override
	public void configure(WebSecurity webSecurity) throws Exception {
		webSecurity.ignoring().antMatchers("/public/**", "/css/**", "/js/**");
		super.configure(webSecurity);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// AuthenticationManagerBuilder is created by the method of
		// WebSecurityConfigurerAdapter.setObjectPostProcessor and by setting different
		// service, the corresponding AuthenticationConfiguration will return.
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());

		// If add super statement, the password encoder will invalid
		// super.configure(auth);
	}

	private PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	private Map<String, String> getUrlPermissonMap() {
		List<SysResource> srList = userDetailsService.obtainResourceList();

		Map<String, String> urlPermissonMap = new HashMap<>();

		for (SysResource r : srList) {
			if (r.getResourceUrl() != null) {
				urlPermissonMap.put(r.getResourceUrl(), r.getPermissions());
			}
		}

		return urlPermissonMap;
	}

}
