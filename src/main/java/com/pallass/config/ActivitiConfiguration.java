package com.pallass.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.activiti.engine.DynamicBpmnService;
import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.spring.ProcessEngineFactoryBean;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import com.houor.bpm.activiti.utils.BpmTaskUtilsImpl;
import com.houor.common.utils.BpmTaskUtils;

/**
 * Activiti配置
 * 
 * @author Houor
 *
 * @createTime 2017-08-05 11:00
 */
@Configuration
public class ActivitiConfiguration {

	/**
	 * Activiti Engine 配置
	 * 
	 * @param dataSource
	 * @param entityManagerFactory
	 * @param transactionManager
	 * @return
	 */
	@Bean
	public SpringProcessEngineConfiguration processEngineConfiguration(DataSource dataSource, EntityManagerFactory entityManagerFactory,
			PlatformTransactionManager transactionManager) {

		SpringProcessEngineConfiguration processEngineConfiguration = new SpringProcessEngineConfiguration();
		processEngineConfiguration.setProcessEngineName("SpringActiviti");

		processEngineConfiguration.setDataSource(dataSource);
		processEngineConfiguration.setTransactionManager(transactionManager);
		processEngineConfiguration.setJpaEntityManagerFactory(entityManagerFactory);
		processEngineConfiguration.setJpaHandleTransaction(false);
		processEngineConfiguration.setJpaCloseEntityManager(false);

		processEngineConfiguration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

		processEngineConfiguration.setJobExecutorActivate(false);

		processEngineConfiguration.setActivityFontName("宋体");
		processEngineConfiguration.setLabelFontName("宋体");

		return processEngineConfiguration;

	}

	/**
	 * Activiti Engine Factory
	 * 
	 * @param processEngineConfiguration
	 * @return
	 */
	@Bean
	ProcessEngineFactoryBean processEngine(SpringProcessEngineConfiguration processEngineConfiguration) {
		ProcessEngineFactoryBean processEngineFactory = new ProcessEngineFactoryBean();
		processEngineFactory.setProcessEngineConfiguration(processEngineConfiguration);

		return processEngineFactory;
	}

	// Activiti服务接口

	@Bean
	public RuntimeService runtimeService(ProcessEngine processEngine) {
		return processEngine.getRuntimeService();
	}

	@Bean
	public RepositoryService repositoryService(ProcessEngine processEngine) {
		return processEngine.getRepositoryService();
	}

	@Bean
	public FormService formService(ProcessEngine processEngine) {
		return processEngine.getFormService();
	}

	@Bean
	public TaskService taskService(ProcessEngine processEngine) {
		return processEngine.getTaskService();
	}

	@Bean
	public HistoryService historyService(ProcessEngine processEngine) {
		return processEngine.getHistoryService();
	}

	@Bean
	public IdentityService identityService(ProcessEngine processEngine) {
		return processEngine.getIdentityService();
	}

	@Bean
	public ManagementService managementService(ProcessEngine processEngine) {
		return processEngine.getManagementService();
	}

	@Bean
	public DynamicBpmnService dynamicBpmnService(ProcessEngine processEngine) {
		return processEngine.getDynamicBpmnService();
	}

	@Bean
	public BpmTaskUtils bpmTaskUtils() {
		return new BpmTaskUtilsImpl();
	}

	// Activiti服务接口

}
