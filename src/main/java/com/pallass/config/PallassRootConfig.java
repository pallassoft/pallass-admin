package com.pallass.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.houor.common.repository.BaseRepositoryFactoryBean;

/**
 * @author Houor
 *
 * @createTime: 2016-12-20 21:24
 */
@Configuration
@EnableAspectJAutoProxy
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "com.pallass.*.repository", "com.houor.bpm.activiti.repository",
		"com.pallass.modules.srm.repository" }, repositoryFactoryBeanClass = BaseRepositoryFactoryBean.class)
@ComponentScan(basePackages = { "com.pallass.admin.service.impl", "com.pallass.security.service", "com.pallass.organization.service.impl", "com.houor.bpm.activiti.service.impl",
		"com.pallass.modules.srm.service.impl" }, excludeFilters = { @ComponentScan.Filter(type = FilterType.ANNOTATION, value = EnableWebMvc.class) })
public class PallassRootConfig {
}
