package com.pallass.utils;

import java.util.HashMap;
import java.util.Map;

public class RequestResult extends HashMap<String, Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5670707584630982285L;

	public static final int CODE_SUCCESS = 0;
	public static final int CODE_FAILURE = -1;

	public RequestResult() {
		put("code", CODE_SUCCESS);
	}

	public static RequestResult failure() {
		return failure(CODE_FAILURE, "未知异常，请联系管理员");
	}

	public static RequestResult failure(String message) {
		return failure(CODE_FAILURE, message);
	}

	public static RequestResult failure(int code, String message) {
		RequestResult RequestResult = new RequestResult();
		RequestResult.put("code", code);
		RequestResult.put("message", message);
		return RequestResult;
	}

	public static RequestResult success() {
		return new RequestResult();
	}

	public static RequestResult success(String message) {
		RequestResult RequestResult = new RequestResult();
		RequestResult.put("message", message);
		return RequestResult;
	}

	public static RequestResult success(Map<String, Object> map) {
		RequestResult RequestResult = new RequestResult();
		RequestResult.putAll(map);
		return RequestResult;
	}

	@Override
	public RequestResult put(String key, Object value) {
		super.put(key, value);
		return this;
	}

}
