package com.pallass.utils;

public class PallassConstants {
	// plain text is 111111.
	// the encode password is not unique
	public final static String INITIAL_PASSWORD = "$2a$10$WbXhTLeXsXpQuxMcHIT.5eTpfFJeg0VP1CFvsOronqT9s6cDXgn4q";

}
