package com.pallass.organization.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.houor.common.controller.BaseController;
import com.houor.common.data.jpa.criteria.OperatorType;
import com.houor.common.data.jpa.domain.ListSpecificationBuilder;
import com.pallass.organization.domain.Clazz;
import com.pallass.organization.service.ClazzService;
import com.pallass.utils.RequestResult;

/**
 * 班级
 * 
 * @author Houor
 *
 * @createTime: 2017-03-08 21:33
 */
@Controller
@RequestMapping("/org/clazz")
public class ClazzController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(ClazzController.class);

	@Autowired
	private ClazzService clazzService;

	@RequestMapping(value = "/index")
	public String index(Model model) {
		return "org/clazzes";
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public Page<Clazz> list() {

		ListSpecificationBuilder<Clazz> builder = new ListSpecificationBuilder<>();
		String clazzName = request.getParameter("clazzName");
		if (StringUtils.isNotBlank(clazzName)) {
			builder.add("clazzName", OperatorType.LIKE, clazzName);
		}

		return clazzService.findAll(builder.generateListSpecification(), getPageRequest());
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public RequestResult save(@RequestBody Clazz clazz) {
		if (StringUtils.isBlank(clazz.getClazzName())) {
			return RequestResult.failure("班级名称不能为空");
		}
		if (StringUtils.isBlank(clazz.getClazzNo())) {
			return RequestResult.failure("班级编号不能为空");
		}
		if (StringUtils.isBlank(clazz.getEnrollYear())) {
			return RequestResult.failure("入学年份不能为空");
		}
		if (clazz.getDepartment() == null || clazz.getDepartment().getId() == 0) {
			return RequestResult.failure("班级所属部门不能为空");
		}

		clazzService.save(clazz);
		logger.debug("添加班级成功");
		return RequestResult.success("添加班级成功");
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Clazz edit(@PathVariable Integer id) {
		return clazzService.findOne(id);
	}

	@RequestMapping(value = "/update")
	@ResponseBody
	public RequestResult update(@RequestBody Clazz clazz) {
		if (StringUtils.isBlank(clazz.getClazzName())) {
			return RequestResult.failure("班级名称不能为空");
		}
		if (StringUtils.isBlank(clazz.getClazzNo())) {
			return RequestResult.failure("班级编号不能为空");
		}
		if (StringUtils.isBlank(clazz.getEnrollYear())) {
			return RequestResult.failure("入学年份不能为空");
		}
		if (clazz.getDepartment() == null || clazz.getDepartment().getId() == 0) {
			return RequestResult.failure("班级所属部门不能为空");
		}

		clazzService.update(clazz);
		return RequestResult.success("修改班级成功");
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public RequestResult delete(@RequestBody Integer ids[]) {
		try {
			clazzService.deleteByIdArray(ids);
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			logger.info("DataIntegrityViolationException: Clazz delete failure.");
			return RequestResult.failure("该班级还有学生, 请确认后再行删除");
		}
		return RequestResult.success("删除班级成功");
	}

}
