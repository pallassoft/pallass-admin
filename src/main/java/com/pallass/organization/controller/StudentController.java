package com.pallass.organization.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.houor.common.controller.BaseController;
import com.houor.common.data.jpa.criteria.OperatorType;
import com.houor.common.data.jpa.domain.ListSpecificationBuilder;
import com.pallass.organization.domain.Clazz;
import com.pallass.organization.domain.Student;
import com.pallass.organization.service.ClazzService;
import com.pallass.organization.service.StudentService;
import com.pallass.utils.RequestResult;

/**
 * 学生
 * 
 * @author Houor
 *
 * @createTime: 2017-03-09 18:04
 */
@Controller
@RequestMapping("/org/student")
public class StudentController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(StudentController.class);

	@Autowired
	private StudentService studentService;

	@Autowired
	private ClazzService clazzService;

	@RequestMapping(value = "/index")
	public String index(Model model) {
		return "org/student";
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public Page<Student> list() {

		ListSpecificationBuilder<Student> builder = new ListSpecificationBuilder<>();
		String studentName = request.getParameter("studentName");
		if (StringUtils.isNotBlank(studentName)) {
			builder.add("userName", OperatorType.LIKE, studentName);
		}

		return studentService.findAll(builder.generateListSpecification(), getPageRequest());
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public RequestResult save(@RequestBody Student student) {
		RequestResult validation = validate(student);
		if (validation != null) {
			return validation;
		}

		studentService.save(student);
		logger.debug("添加学生成功");
		return RequestResult.success("添加学生成功");
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Student edit(@PathVariable Integer id) {
		return studentService.findOne(id);
	}

	@RequestMapping(value = "/update")
	@ResponseBody
	public RequestResult update(@RequestBody Student student) {

		RequestResult validation = validate(student);
		if (validation != null) {
			return validation;
		}

		studentService.update(student);
		return RequestResult.success("修改学生成功");
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public RequestResult delete(@RequestBody Integer ids[]) {
		try {
			studentService.deleteByIdArray(ids);
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			logger.info("DataIntegrityViolationException: Student delete failure.");
			return RequestResult.failure("该学生已被引用, 请确认后再行删除");
		}
		return RequestResult.success("删除学生成功");
	}

	@RequestMapping(value = "/clazzes/{deptId}", method = RequestMethod.GET)
	@ResponseBody
	public List<Clazz> getClazzList(@PathVariable Integer deptId) {

		List<Clazz> result = new ArrayList<>();

		if (deptId != 0) {
			List<Clazz> clazzList = clazzService.findByDepartmentId(deptId);
			if (clazzList != null && !clazzList.isEmpty()) {
				result.addAll(clazzList);
			}
		}

		return result;
	}

	private RequestResult validate(Student student) {
		if (StringUtils.isBlank(student.getUserName())) {
			return RequestResult.failure("学号不能为空");
		}

		if (student.getDepartment() == null || student.getDepartment().getId() == 0) {
			return RequestResult.failure("学生所属部门不能为空");
		}

		if (student.getClazz() == null || student.getClazz().getId() == 0) {
			return RequestResult.failure("学生所属班级不能为空");
		}

		if (StringUtils.isBlank(student.getEmail())) {
			return RequestResult.failure("Email不能为空");
		}

		return null;
	}

}
