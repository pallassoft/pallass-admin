package com.pallass.organization.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.houor.common.controller.BaseController;
import com.houor.common.data.jpa.criteria.OperatorType;
import com.houor.common.data.jpa.domain.ListSpecificationBuilder;
import com.pallass.organization.domain.Department;
import com.pallass.organization.service.DepartmentService;
import com.pallass.utils.RequestResult;

/**
 * 部门
 * 
 * @author Houor
 *
 * @createTime: 2017-03-08 12:13
 */
@Controller
@RequestMapping("/org/dept")
public class DepartmentController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(DepartmentController.class);

	@Autowired
	private DepartmentService departmentService;

	@RequestMapping(value = "/index")
	public String index(Model model) {
		return "org/department";
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public Page<Department> list() {

		ListSpecificationBuilder<Department> builder = new ListSpecificationBuilder<>();
		String deptName = request.getParameter("deptName");
		if (StringUtils.isNotBlank(deptName)) {
			builder.add("deptName", OperatorType.LIKE, deptName);
		}

		return departmentService.findAll(builder.generateListSpecification(), getPageRequest());
	}

	@RequestMapping(value = "/lists")
	@ResponseBody
	public RequestResult lists(ModelMap map) {
		List<Department> results = departmentService.findAll();

		// 构造父子树
		Department root = new Department();
		root.setId(0);
		root.setDeptName("顶级部门");
		root.setOpen(true);

		for (Department d : results) {
			if (d.getParent() == null) {
				root.getChildren().add(d);
			} else {
				d.getParent().getChildren().add(d);
				d.setParent(null);
			}
		}

		return RequestResult.success().put("root", root);
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public RequestResult save(@RequestBody Department dept) {
		if (StringUtils.isBlank(dept.getDeptName())) {
			return RequestResult.failure("部门名称不能为空");
		}
		if (StringUtils.isBlank(dept.getDeptNo())) {
			return RequestResult.failure("部门编码不能为空");
		}

		// clear root parent
		if (dept.getParent().getId() == 0)
			dept.setParent(null);

		departmentService.save(dept);
		logger.debug("添加部门成功");
		return RequestResult.success("添加部门成功");
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Department edit(@PathVariable Integer id) {
		return departmentService.findOne(id);
	}

	@RequestMapping(value = "/update")
	@ResponseBody
	public RequestResult update(@RequestBody Department dept) {
		if (StringUtils.isBlank(dept.getDeptName())) {
			return RequestResult.failure("部门名称不能为空");
		}
		if (StringUtils.isBlank(dept.getDeptNo())) {
			return RequestResult.failure("部门编码不能为空");
		}
		departmentService.update(dept);
		return RequestResult.success("修改部门成功");
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public RequestResult delete(@RequestBody Integer ids[]) {
		try {
			departmentService.deleteByIdArray(ids);
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			logger.info("DataIntegrityViolationException: Department delete failure.");
			return RequestResult.failure("部门有下级部门, 请确认后再行删除");
		}
		return RequestResult.success("删除部门成功");
	}

	// FIXME 部门选择时，可以选择为自身，造成循环依赖，且部门编辑时，不能显示部门信息

}
