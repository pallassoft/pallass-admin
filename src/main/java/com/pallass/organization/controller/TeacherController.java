package com.pallass.organization.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.houor.common.controller.BaseController;
import com.houor.common.data.jpa.criteria.OperatorType;
import com.houor.common.data.jpa.domain.ListSpecificationBuilder;
import com.pallass.organization.domain.Teacher;
import com.pallass.organization.service.TeacherService;
import com.pallass.utils.RequestResult;

/**
 * 教师
 * 
 * @author Houor
 *
 * @createTime: 2017-03-09 19:07
 */
@Controller
@RequestMapping("/org/teacher")
public class TeacherController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(TeacherController.class);

	@Autowired
	private TeacherService teacherService;

	@RequestMapping(value = "/index")
	public String index(Model model) {
		return "org/teacher";
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public Page<Teacher> list() {

		ListSpecificationBuilder<Teacher> builder = new ListSpecificationBuilder<>();
		String teacherName = request.getParameter("teacherName");
		if (StringUtils.isNotBlank(teacherName)) {
			builder.add("userName", OperatorType.LIKE, teacherName);
		}

		return teacherService.findAll(builder.generateListSpecification(), getPageRequest());
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public RequestResult save(@RequestBody Teacher teacher) {
		RequestResult validation = validate(teacher);
		if (validation != null) {
			return validation;
		}

		teacherService.save(teacher);
		logger.debug("添加教师成功");
		return RequestResult.success("添加教师成功");
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Teacher edit(@PathVariable Integer id) {
		return teacherService.findOne(id);
	}

	@RequestMapping(value = "/update")
	@ResponseBody
	public RequestResult update(@RequestBody Teacher teacher) {

		RequestResult validation = validate(teacher);
		if (validation != null) {
			return validation;
		}

		teacherService.update(teacher);
		return RequestResult.success("修改教师成功");
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public RequestResult delete(@RequestBody Integer ids[]) {
		try {
			teacherService.deleteByIdArray(ids);
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			logger.info("DataIntegrityViolationException: Teacher delete failure.");
			return RequestResult.failure("该教师已被引用 请确认后再行删除");
		}
		return RequestResult.success("删除教师成功");
	}

	private RequestResult validate(Teacher teacher) {
		if (StringUtils.isBlank(teacher.getUserName())) {
			return RequestResult.failure("教师工号不能为空");
		}
		if (StringUtils.isBlank(teacher.getEmail())) {
			return RequestResult.failure("教师Email不能为空");
		}

		if (teacher.getDepartment() == null || teacher.getDepartment().getId() == 0) {
			return RequestResult.failure("教师所属部门不能为空");
		}

		return null;
	}

}
