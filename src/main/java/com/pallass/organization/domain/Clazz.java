package com.pallass.organization.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.houor.common.domain.BaseDomain;

/**
 * 班级
 * 
 * @author Houor
 *
 * @createTime: 2017-03-08 21:19
 */
@Entity
@Table(name = "sys_clazz")
public class Clazz extends BaseDomain implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -704188218618843879L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department_id", foreignKey = @ForeignKey(name = "fk_clazz_department_id"))
	private Department department;

	/**
	 * 班级编号
	 */
	private String clazzNo;

	/**
	 * 班级名称
	 */
	private String clazzName;

	/**
	 * 入学年份
	 */
	private String enrollYear;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getClazzNo() {
		return clazzNo;
	}

	public void setClazzNo(String clazzNo) {
		this.clazzNo = clazzNo;
	}

	public String getClazzName() {
		return clazzName;
	}

	public void setClazzName(String clazzName) {
		this.clazzName = clazzName;
	}

	public String getEnrollYear() {
		return enrollYear;
	}

	public void setEnrollYear(String enrollYear) {
		this.enrollYear = enrollYear;
	}

}
