package com.pallass.organization.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.houor.common.domain.BaseDomain;

/**
 * 部门
 * 
 * @author Houor
 *
 * @createTime: 2017-03-08 11:50
 */
@Entity
@Table(name = "sys_department")
public class Department extends BaseDomain implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6283108386337824057L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", foreignKey = @ForeignKey(name = "fk_sys_department_parent"))
	private Department parent;

	@Transient
	private List<Department> children = new ArrayList<Department>();

	/**
	 * 父菜单ID，一级菜单为0, 由上级对象parent生产
	 */
	@Transient
	private Integer parentId;

	/**
	 * 父菜单名称, 由上级对象parent获得
	 */
	@Transient
	private String parentName;

	/**
	 * 部门名称
	 */
	private String deptName;

	/**
	 * 部门编码
	 */
	private String deptNo;

	/**
	 * 部门地址
	 */
	private String deptAddress;

	/**
	 * 邮政编码
	 */
	private String zipCode;

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 电话
	 */
	private String telephone;

	/**
	 * 传真
	 */
	private String fax;

	/**
	 * 是否锁定
	 * 
	 * 0 未锁定 1 锁定
	 */
	private Integer locked;

	@Transient
	private boolean open;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Department getParent() {
		return parent;
	}

	public void setParent(Department parent) {
		this.parent = parent;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptAddress() {
		return deptAddress;
	}

	public void setDeptAddress(String deptAddress) {
		this.deptAddress = deptAddress;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public Integer getLocked() {
		return locked;
	}

	public void setLocked(Integer locked) {
		this.locked = locked;
	}

	public Integer getParentId() {
		return parent != null ? parent.getId() : 0;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parent != null ? parent.getId() : 0;
	}

	public String getParentName() {
		return parent != null ? parent.getDeptName() : "顶级部门节点";
	}

	public void setParentName(String parentName) {
		this.parentName = parent != null ? parent.getDeptName() : "顶级部门节点";
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public List<Department> getChildren() {
		return children;
	}

	public void setChildren(List<Department> children) {
		this.children = children;
	}
}
