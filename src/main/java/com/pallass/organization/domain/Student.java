package com.pallass.organization.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pallass.common.domain.Person;

/**
 * 学生
 * 
 * @author Houor
 *
 * @createTime: 2017-03-09 18:41
 */
@Entity
@Table(name = "sys_student")
public class Student extends Person implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5971379593757590842L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_dept", foreignKey = @ForeignKey(name = "fk_student_department_id"))
	private Department department;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_clazz", foreignKey = @ForeignKey(name = "fk_student_clazz_id"))
	private Clazz clazz;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Clazz getClazz() {
		return clazz;
	}

	public void setClazz(Clazz clazz) {
		this.clazz = clazz;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

}
