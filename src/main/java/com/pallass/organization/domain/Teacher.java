package com.pallass.organization.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pallass.common.domain.Person;

/**
 * 教师
 * 
 * @author Houor
 *
 * @createTime: 2017-03-09 11:08
 */
@Entity
@Table(name = "sys_teacher")
public class Teacher extends Person implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -815182611436642611L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_dept", foreignKey = @ForeignKey(name = "fk_teacher_department_id"))
	private Department department;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

}
