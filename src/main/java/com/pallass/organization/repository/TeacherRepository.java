package com.pallass.organization.repository;

import org.springframework.stereotype.Repository;

import com.houor.common.repository.BaseRepository;
import com.pallass.organization.domain.Teacher;

@Repository
public interface TeacherRepository extends BaseRepository<Teacher, Integer> {

}
