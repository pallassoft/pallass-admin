package com.pallass.organization.repository;

import org.springframework.stereotype.Repository;

import com.houor.common.repository.BaseRepository;
import com.pallass.organization.domain.Student;

@Repository
public interface StudentRepository extends BaseRepository<Student, Integer> {

}
