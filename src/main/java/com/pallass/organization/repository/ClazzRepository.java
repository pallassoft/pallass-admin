package com.pallass.organization.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.houor.common.repository.BaseRepository;
import com.pallass.organization.domain.Clazz;

@Repository
public interface ClazzRepository extends BaseRepository<Clazz, Integer> {

	List<Clazz> findByDepartmentId(Integer deptId);

}
