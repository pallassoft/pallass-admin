package com.pallass.organization.repository;

import org.springframework.stereotype.Repository;

import com.houor.common.repository.BaseRepository;
import com.pallass.organization.domain.Department;

@Repository
public interface DepartmentRepository extends BaseRepository<Department, Integer> {

}
