package com.pallass.organization.service;

import com.houor.common.service.BaseService;
import com.pallass.organization.domain.Department;

public interface DepartmentService extends BaseService<Department, Integer> {

}
