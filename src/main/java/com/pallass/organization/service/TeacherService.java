package com.pallass.organization.service;

import com.houor.common.service.BaseService;
import com.pallass.organization.domain.Teacher;

public interface TeacherService  extends BaseService<Teacher, Integer> {

}
