package com.pallass.organization.service;

import java.util.List;

import com.houor.common.service.BaseService;
import com.pallass.organization.domain.Clazz;

public interface ClazzService extends BaseService<Clazz, Integer> {

	List<Clazz> findByDepartmentId(Integer deptId);

}
