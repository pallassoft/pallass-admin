package com.pallass.organization.service;

import com.houor.common.service.BaseService;
import com.pallass.organization.domain.Student;

public interface StudentService extends BaseService<Student, Integer> {

}
