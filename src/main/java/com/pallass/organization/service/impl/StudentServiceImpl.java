package com.pallass.organization.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.houor.common.repository.BaseRepository;
import com.houor.common.service.BaseServiceImpl;
import com.pallass.organization.domain.Student;
import com.pallass.organization.repository.StudentRepository;
import com.pallass.organization.service.StudentService;

@Service("studentService")
public class StudentServiceImpl extends BaseServiceImpl<Student, Integer> implements StudentService {

	@Autowired
	StudentRepository studentRepository;

	@Override
	public BaseRepository<Student, Integer> getRepository() {
		return this.studentRepository;
	}

}
