package com.pallass.organization.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.houor.common.repository.BaseRepository;
import com.houor.common.service.BaseServiceImpl;
import com.pallass.organization.domain.Department;
import com.pallass.organization.repository.DepartmentRepository;
import com.pallass.organization.service.DepartmentService;

@Service("departmentService")
public class DepartmentServiceImpl extends BaseServiceImpl<Department, Integer> implements DepartmentService {

	@Autowired
	DepartmentRepository departmentRepository;

	@Override
	public BaseRepository<Department, Integer> getRepository() {
		return this.departmentRepository;
	}

}
