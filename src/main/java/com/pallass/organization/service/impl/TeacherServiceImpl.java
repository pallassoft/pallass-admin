package com.pallass.organization.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.houor.common.repository.BaseRepository;
import com.houor.common.service.BaseServiceImpl;
import com.pallass.organization.domain.Teacher;
import com.pallass.organization.repository.TeacherRepository;
import com.pallass.organization.service.TeacherService;

@Service("teacherService")
public class TeacherServiceImpl extends BaseServiceImpl<Teacher, Integer> implements TeacherService {

	@Autowired
	TeacherRepository teacherRepository;

	@Override
	public BaseRepository<Teacher, Integer> getRepository() {
		return this.teacherRepository;
	}

}
