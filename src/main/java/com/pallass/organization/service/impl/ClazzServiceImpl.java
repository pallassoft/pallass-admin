package com.pallass.organization.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.houor.common.repository.BaseRepository;
import com.houor.common.service.BaseServiceImpl;
import com.pallass.organization.domain.Clazz;
import com.pallass.organization.repository.ClazzRepository;
import com.pallass.organization.service.ClazzService;

@Service("clazzService")
public class ClazzServiceImpl extends BaseServiceImpl<Clazz, Integer> implements ClazzService {

	@Autowired
	ClazzRepository clazzRepository;

	@Override
	public BaseRepository<Clazz, Integer> getRepository() {
		return this.clazzRepository;
	}

	@Override
	public List<Clazz> findByDepartmentId(Integer deptId) {
		return clazzRepository.findByDepartmentId(deptId);
	}

}
