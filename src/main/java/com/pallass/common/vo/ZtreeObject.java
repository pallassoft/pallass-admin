package com.pallass.common.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ZtreeObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5933575547717038120L;

	private Integer id;

	private Integer parentId;

	private String name;

	private boolean open;

	private boolean checked = false;

	private List<ZtreeObject> children = new ArrayList<ZtreeObject>();

	public ZtreeObject() {
	}

	public ZtreeObject(Integer id, Integer parentId, String name, boolean open, boolean checked) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.name = name;
		this.open = open;
		this.checked = checked;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public List<ZtreeObject> getChildren() {
		return children;
	}

	public void setChildren(List<ZtreeObject> children) {
		this.children = children;
	}

}
