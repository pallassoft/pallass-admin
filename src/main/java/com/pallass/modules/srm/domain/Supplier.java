package com.pallass.modules.srm.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.houor.common.domain.BpmBaseDomain;

/**
 * 供应商
 * 
 * @author Houor
 *
 * @createTime 2017-08-16 17:18
 */

@Entity
@Table(name = "srm_supplier")
public class Supplier extends BpmBaseDomain implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2661999892637723748L;

	private String supplierName;// 供应商名称

	private String supplierEmail;// 供应商Email

	private String supplierWeb;// 供应商网址

	private String supplierTel;// 供应商固定电话

	private String usertaskAudit; // 审批意见

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierEmail() {
		return supplierEmail;
	}

	public void setSupplierEmail(String supplierEmail) {
		this.supplierEmail = supplierEmail;
	}

	public String getSupplierWeb() {
		return supplierWeb;
	}

	public void setSupplierWeb(String supplierWeb) {
		this.supplierWeb = supplierWeb;
	}

	public String getSupplierTel() {
		return supplierTel;
	}

	public void setSupplierTel(String supplierTel) {
		this.supplierTel = supplierTel;
	}

	public String getUsertaskAudit() {
		return usertaskAudit;
	}

	public void setUsertaskAudit(String usertaskAudit) {
		this.usertaskAudit = usertaskAudit;
	}

}
