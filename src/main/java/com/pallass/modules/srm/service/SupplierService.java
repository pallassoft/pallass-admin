package com.pallass.modules.srm.service;

import com.houor.common.service.BpmBaseService;
import com.pallass.modules.srm.domain.Supplier;

public interface SupplierService extends BpmBaseService<Supplier, Integer> {

}
