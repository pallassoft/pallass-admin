package com.pallass.modules.srm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.houor.common.repository.BaseRepository;
import com.houor.common.service.BpmBaseServiceImpl;
import com.pallass.modules.srm.domain.Supplier;
import com.pallass.modules.srm.repository.SupplierRepository;
import com.pallass.modules.srm.service.SupplierService;

@Service("supplierService")
public class SupplierServiceImpl extends BpmBaseServiceImpl<Supplier, Integer> implements SupplierService {

	@Autowired
	SupplierRepository supplierRepository;

	@Override
	public BaseRepository<Supplier, Integer> getRepository() {
		return supplierRepository;
	}

}
