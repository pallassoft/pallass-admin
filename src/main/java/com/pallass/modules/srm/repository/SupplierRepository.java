package com.pallass.modules.srm.repository;

import org.springframework.stereotype.Repository;

import com.houor.common.repository.BpmBaseRepository;
import com.pallass.modules.srm.domain.Supplier;

@Repository
public interface SupplierRepository extends BpmBaseRepository<Supplier, Integer> {
	
	

}
