package com.pallass.modules.srm.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.houor.common.controller.BpmBaseController;
import com.houor.common.data.jpa.criteria.OperatorType;
import com.houor.common.data.jpa.domain.ListSpecificationBuilder;
import com.houor.common.service.BpmBaseService;
import com.pallass.modules.srm.domain.Supplier;
import com.pallass.modules.srm.service.SupplierService;

/**
 * 供应商Controller
 * 
 * @author Houor
 *
 * @createTime 2017-08-15 22:13
 */
@Controller
@RequestMapping("/srm/supplier")
public class SupplierController extends BpmBaseController<Supplier> {

	private static Logger logger = LoggerFactory.getLogger(SupplierController.class);

	@Autowired
	SupplierService supplierService;

	public BpmBaseService<Supplier, Integer> getBpmBaseService() {
		return this.supplierService;
	}

	@RequestMapping(value = "/index")
	public String index(Model model) {
		model.addAttribute("purl", "/srm/supplier");
		return "/srm/supplier";
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public Page<Supplier> list() {

		ListSpecificationBuilder<Supplier> builder = new ListSpecificationBuilder<>();
		String supplierName = request.getParameter("supplierName");
		if (StringUtils.isNotBlank(supplierName)) {
			builder.add("supplierName", OperatorType.LIKE, supplierName);
		}

		return supplierService.findAll(builder.generateListSpecification(), getPageRequest());
	}

	@RequestMapping(value = "/save")
	public String save(Supplier supplier) {

		supplierService.startOrTodoTask(supplier);

		return "/srm/supplier";
	}

	@RequestMapping(value = "/form")
	public String form(Supplier supplier, Model model) {

		// 任务开始
		if ("start".equals(supplier.getStatus())) {
			logger.debug("Create instance for process: {}", supplier.getProcessDefinitionId());
		}
		// 办理任务
		else if ("todo".equals(supplier.getStatus())) {
			logger.debug("Handle task: {} for process: {}", supplier.getTaskId(), supplier.getProcessDefinitionId());
			supplier = handleTaskInfo(supplier);
		}
		model.addAttribute("supplier", supplier);
		return "srm/supplierForm";
	}

}
