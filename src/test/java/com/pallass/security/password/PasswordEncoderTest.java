package com.pallass.security.password;

import org.junit.Assert;
import org.junit.Test;

import com.pallass.security.util.BCryptUtil;

public class PasswordEncoderTest {

	@Test
	public void testBCryptPasswordEncoderAndMatch() {

		String plainText = "111111";
		String encodeText = BCryptUtil.generateBcryptPasswd(plainText);
		System.out.println(encodeText);
		Assert.assertNotEquals(plainText, encodeText);
		Assert.assertTrue(BCryptUtil.compare(plainText, encodeText));

	}

}
